#!/usr/bin/env python
# coding: utf8

from musique.basicsynth import BasicSequenceur



s = BasicSequenceur(1, 117, 0.7, 3)

# voir par exemple http://www.lalecondepiano.com/astuces/liste-acc-test76.htm

notesDeuxMains = [ 
  # première main, avec les accords
  [ [ [ s.re(2), s.fa(2), s.la(2)], s.blanche ],
    [ [ s.re(2), s.fa(2), s.la(2)], s.blanche ],
    [ [ s.re(2), s.fa(2), s.la(2)], s.blanche ] ],

  # deuxième main, avec la mélodie
  [ [ s.re(3), s.croche ], [ s.re(3), s.croche ], [ s.re(3), s.croche ], [ s.re(3), s.croche ], 
    [ s.fa(3), s.croche ], [ s.fa(3), s.croche ], [ s.fa(3), s.croche ], [ s.fa(3), s.croche ],
    [ s.la(3), s.croche ], [ s.la(3), s.croche ], [ s.la(3), s.croche ], [ s.la(3), s.croche ] ] ]
          

s.genererMelodie(notesDeuxMains, "sons/notesDeuxMains.wav")
