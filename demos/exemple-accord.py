#!/usr/bin/env python
# coding: utf8

from musique.basicsynth import BasicSequenceur



s = BasicSequenceur(1, 117, 0.7, 3)

# voir par exemple http://www.lalecondepiano.com/astuces/liste-acc-test76.htm

notesAvecAccord = [ [ 
  [ [ s.re(), s.fa(), s.la()], s.blanche ],
  [ [ s.re(), s.fa(), s.la()], s.blanche ],
  [ [ s.re(), s.fa(), s.la()], s.blanche ]
    ] ]
          

s.genererMelodie(notesAvecAccord, "sons/notesAvecAccord.wav")
