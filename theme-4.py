#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème Lullaby, berceuse de Brahms.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(117)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


brahms = [ [ [gamme.mi(3), gamme.croche],
             [gamme.mi(3), gamme.croche],
             [gamme.sol(3), gamme.noirepointee],
             [gamme.mi(3), gamme.croche],
             [gamme.mi(3), gamme.noire],
             [gamme.sol(3), gamme.blanche],
             [gamme.mi(3), gamme.croche],
             [gamme.sol(3), gamme.croche],
             [gamme.do(4), gamme.noire],
             [gamme.si(3), gamme.noire],
             [gamme.la(3), gamme.noire],
             [gamme.la(3), gamme.noire],
             [gamme.sol(3), gamme.noire],

             
             [gamme.re(3), gamme.croche],
             [gamme.mi(3), gamme.croche],
             [gamme.fa(3), gamme.noire],
             [gamme.re(3), gamme.noire],
             [gamme.re(3), gamme.croche],
             [gamme.mi(3), gamme.croche],
             [gamme.fa(3), gamme.blanche],
             
             [gamme.re(3), gamme.croche],
             [gamme.fa(3), gamme.croche],
             [gamme.si(3), gamme.croche],
             [gamme.la(3), gamme.croche],
             [gamme.sol(3), gamme.noire],
             [gamme.si(3), gamme.noire],
             [gamme.do(4), gamme.blanche],
             
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(6)
  g.setLongueurPartitionMusicale(12)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(brahms, args.exerciceSpectrogramme[0], "thème 4")
  if args.transparent:
    g.genererTransparentSpectrogramme(brahms, args.transparent[0], "thème 4", "Lullaby, berceuse de Brahms")
  if args.correction:
    g.genererCorrectionSpectrogramme(brahms, args.correction[0], "thème 4", "Lullaby, berceuse de Brahms")
  
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(brahms, "sons/brahms.wav")

