# coding: utf8
# -*- coding: utf-8 -*-

from gamme import GammeTemperee



# Cette classe permet de générer des exercices d'analyse de spectrogramme au 
# format LaTeX à partir d'une partition donnée en paramètre.
class GenerateurExercice(GammeTemperee):
  
    # constructeur par defaut de la classe.
    # Paramètres:
    # * bpm: nombre de battements par minute (= nombre de noires par minute)
    # * dureeFrappe: duree de la frappe
    def __init__(self, bpm = 120, dureeFrappe = 0.12):
        GammeTemperee.__init__(self, bpm)
        self.dureeFrappe = dureeFrappe
        self.margeDocument = 1.0
        self.margeDureeDebut = 0.
        self.margeDureeFin = 0.15
        self.margeFrequence = 100 
        self.margeFrequenceLegende = 20
        self.margeLegende = 2.0
        self.boomwhackers = []
        self.longueurPartitionColoree = 4
        self.longueurPartitionMusicale = 16
        self.tableauSimplifie = False


    def setMinMaxOctaves(self, partitions):
      notes = self.getOctaveEtIDNoteDansIntervalle(self.minFrequence(partitions), self.maxFrequence(partitions))
      self.minOctave = min([n[0] for n in notes])
      self.maxOctave = max([n[0] for n in notes])

    def setTableauSimplifie(self, simplifie):
      self.tableauSimplifie = simplifie

    # donne la largeur d'une ligne de partition colorée, exprimée
    # en nombre de noires
    def setLongueurPartitionColoree(self, longueur):
      self.longueurPartitionColoree = longueur
    
    # donne la largeur d'une ligne de partition musicale, exprimée
    # en nombre de noires
    def setLongueurPartitionMusicale(self, longueur):
      self.longueurPartitionMusicale = longueur

    def setBoomwhackers(self, mw):
      import json
      with open(mw) as f:
        bw = json.load(f)
        self.boomwhackers = {}
        for key, value in bw.iteritems():
          
          self.boomwhackers[(int(key[-1]), self.getIDNoteDepuisNom(key[:-1]))] = value

    # calcule la durée de la partition donnée en paramètre, dans le 
    # cas où elle est jouée au battement par minute courant
    def calculeDureePartition(self, partition):
      durees = [p[1] for p in partition]
      return sum(durees)

  
    # calcule la durée maximale des partitions données en paramètre, dans le 
    # cas où elles sont jouées au battement par minute courant
    def calculeDureePartitions(self, partitions):
      return max([self.calculeDureePartition(p) for p in partitions])
    
    
    # calcule les fréquences minimales et maximales des partitions
    def calculeIntervalleFrequencesPartitions(self, partitions):
      notes = self.notesDansPartition(partitions)
      return (min(notes), max(notes))
    
  
    # cette fonction calcule l'échelle qui sera utilisée pour le spectrogramme:
    #  - combien de cm de large pour 1 seconde de la partition
    #  - combien de cm de haut pour 1kHz
    # Paramètres:
    # * partitions: la liste des notes qui seront dessinées
    # * largeur: la largeur de la figure en cm
    # * hauteur: la hauteur de la figure en cm
    def initialisationEchelleSpectrogramme(self, partitions, largeur, hauteur):
        self.duree = self.calculeDureePartitions(partitions)
        (self.fmin, self.fmax) = self.calculeIntervalleFrequencesPartitions(partitions)
        
        self.largeur = largeur
        self.hauteur = hauteur

        self.echelleX = largeur / (self.duree + self.margeDureeDebut + self.margeDureeFin)
        self.echelleY = hauteur / (self.fmax - self.fmin + self.margeFrequence * 2 + self.margeFrequenceLegende)
  
  
    # cette fonction retourne l'entête d'un document LaTeX
    def enteteLaTex(self, titre):
        result = "\\documentclass[12pt]{article}\n"
        result += "\\usepackage[utf8]{inputenc}\n"
        result += "\\usepackage[french]{babel}\n"
        result += "\\usepackage{tikz,pgfplots}\n"
        result += "\\usepackage{float}\n"
        result += "\\usepackage[margin=" + str(self.margeDocument) + "cm]{geometry}\n"
        result += "\\usetikzlibrary{positioning}\n"
        result += "\\usetikzlibrary{shapes.geometric}\n"
        result += "\\usetikzlibrary{shapes.symbols}\n"
        result += "\\usetikzlibrary{shadows}\n"
        result += "\\usetikzlibrary{decorations.pathmorphing, patterns,shapes}\n"
        result += "\\usetikzlibrary{positioning, decorations.markings}\n"
        result += "\\usepgflibrary{arrows}\n"
        result += "\\usetikzlibrary{arrows}\n"
        result += "\\pagestyle{empty}\n"
        result += "\\usepackage{helvet}"
        result += "\\usepackage{musixtex}"
        result += "\\usepackage{harmony}"
        result += "\\renewcommand{\\familydefault}{\\sfdefault}"


        result += "\\title{" + titre + "}\n"
        result += "\\date{}\n"
        result += "\\begin{document}\n"
        result += "\\maketitle\n"
        result += "\\thispagestyle{empty}\n"
        return result

    def enteteBeamer(self, titre):
        result = "\\documentclass{beamer}\n"
        result += "\\usepackage[utf8]{inputenc}\n"
        result += "\\usepackage[french]{babel}\n"
        result += "\\usepackage{tikz,pgfplots}\n"
        result += "\\usepackage{float}\n"
        result += "\\usetikzlibrary{positioning}\n"
        result += "\\usetikzlibrary{shapes.geometric}\n"
        result += "\\usetikzlibrary{shapes.symbols}\n"
        result += "\\usetikzlibrary{shadows}\n"
        result += "\\usetikzlibrary{decorations.pathmorphing, patterns,shapes}\n"
        result += "\\usetikzlibrary{positioning, decorations.markings}\n"
        result += "\\usepgflibrary{arrows}\n"
        result += "\\usetikzlibrary{arrows}\n"
        result += "\\pagestyle{empty}\n"
        result += "\\usepackage{helvet}"
        result += "\\usepackage{musixtex}"
        result += "\\usepackage{harmony}"
        result += "\\renewcommand{\\familydefault}{\\sfdefault}"


        result += "\\title{" + titre + "}\n"
        result += "\\date{}\n"
        result += "\\begin{document}\n"
        result += "\\frame{\\titlepage}"
        return result

    # cette fonction retourne le pied d'un document LaTeX
    def piedLaTeX(self):
        return "\\end{document}\n"

    # cette fonction retourne le pied d'un document Beamer
    def piedBeamer(self):
        return "\\end{document}\n"
    # cette fonction écrit le début de l'exercice spectrogramme
    def debutExerciceSpectrogramme(self, section = True):
        result = "\n"
        #result += "\\section{Les fréquences de la gamme tempérée}\n"
        #result += "La fréquence associée à chacune des notes notées sur une partition a varié au fil du temps. La conférence internationale de Londres de 1953 a fixé à 440~Hz la fréquence du $la^3$, ou \emph{$la$ du diapason}. Le système d'accord le plus répandu aujourd'hui est la gamme tempérée, qui divise l'octave en 12 intervalles chromatiques égaux, en s'éloignant légèrement des intonations justes afin de simplifier la gamme tout en permettant les accords conçus aux siècles précédents. Le tableau ci-dessous donne les fréquences pour chacune des notes de l'octave contenant le $la$ du diapason.\n"
        #result += "\\begin{center}\n"
        #result += "{\\scriptsize \\begin{tabular}{r|ccccccccccccc}\n"
        #result += "note & \\textbf{do} & do$\\sharp$ & ré & mi$\\flat$ & mi & fa & fa$\\sharp$ &sol & sol$\\sharp$ & la & si$\\flat$ & si & \\textbf{do} \\\\\n"
        #result += "\\hline\n"
        #result += " Fréquence & 264,00 & 279,70& 296,33 & 313,95& 332,62& 352,40& 373,35& 395,55& 419,07& 443,99& 470,39& 498,37& 528,00 \\\\\n"
        #result += "\\end{tabular}}\n"
        #result += "\\end{center}\n"
        #result += "\n"
        #result += "\n"
        #result += "\n"
        result += "\n"
        if section:
          result += "\\section{Analyse de spectrogramme}\n"
        return result


    
    # retourne un tableau des durées des notes possibles dans la partition
    def tableauDurees(self, partitions, beamer = False):
        result = "\n"
        
        if not beamer:
          if not self.tableauSimplifie:
            result += "Cette partition est proposée à une fréquence de " + str(self.bpm) + " noires par minute, ce qui donne les durées suivantes."
          else:
            result += "Les durées traits sur le graphique peuvent être traduites en durée musicale"
        
        ds = self.durees(partitions)
        

        
        if beamer:
          result += '\\resizebox{1\\textwidth}{!}{%\n'
        else:
          result += "\\begin{table}[H]\n\centering\n"
        result += "\\begin{tabular}{|l" + "".join(["|c" for d in ds ])  + "|}\n"
        result += "\\hline\n"
        
        result += "\\textbf{Graphique} "
        for duree in ds:
          result += "& "
          result += "\\begin{tikzpicture}[]"
          result += self.noteGraphique(0, 0, duree, 0)
          result += "\\end{tikzpicture}"
        result += "\\\\ \n"
        result += "\\hline\n"
        
        if not self.tableauSimplifie and not beamer:
          result += "\\textbf{Longueur} (cm) "
          for duree in ds:
            result += "& " + '{0:.2f}'.format(self.dureeEnCm(duree)).replace(".", ",")
          result += "\\\\ \n"
          result += "\\hline\n"
          
          result += "\\textbf{Durée} (s)"
          for duree in ds:
            result += "& " + '{0:.2f}'.format(duree).replace(".", ",")
          result += "\\\\ \n"
          result += "\\hline\n"
        
        result += "\\textbf{Durée musicale}"
        for duree in ds:
          result += "& " + self.nomDuree(duree)
        result += "\\\\ \n"
        result += "\\hline\n"

        
        result += "\\end{tabular}\n"
        if beamer:
          result += "}\n"
        else:
          result += "\\caption{Table des durées}\n"
          result += "\\end{table}\n"
      
      
        return result
      
    # retourne un tableau LaTeX décrivant la partition sous forme brute (fréquence
    # et durée de chacune des notes)
    def tableauNotesBrutes(self, partitions, solution = False):
        # on ne va utiliser que la première partition
        partition = partitions[0]
        
        self.setShiftOctave(partitions)
        
        result = "\\renewcommand{\\arraystretch}{1.3}"
        result += "\\begin{table}[H]\n\centering\n"
        if len(partition) > 30:
          result += "{\\footnotesize \n"
        result += "\\begin{tabular}{|c||c|c|c|}\n"
        result += "\\hline\n"
        result += " & \multicolumn{2}{c|}{Codage} & Représentation \\\\ \n"
        result += " &\qquad\\textbf{Note}\qquad~ & \qquad\\textbf{Durée}\qquad~ & \\textbf{Graphique}  \\\\ \n"
        result += "\\hline \n"
        for i, note in enumerate(partition):
          result += str(i + 1) + " & "
          if solution or i < 3:
            if isinstance(note[0], float):
              if note[0] > 0:
                result += self.frequenceToLaTeX(note[0], 
                                                self.minOctave, self.maxOctave) +  "& " + self.nomDuree(note[1]) + " & "
                
                # ajout note graphique
                result += "\\begin{tikzpicture}[]"
                nid = self.shiftNote(self.getOctaveEtIDNote(note[0]), self.shiftOctave)
                mw = self.boomwhackers[nid]
                if len(partition) > 30:
                  result += self.pointColore(mw, 0, 0, 0.15)
                else:
                  result += self.pointColore(mw, 0, 0, 0.2)
                result += "\\end{tikzpicture}"
                
                result += " \\\\\n"
                result += "\\hline\n"
              else:
                result += " & " + self.nomDuree(note[1]) + " & \\\\\n"
                result += "\\hline\n"
            else:
              result += " & " + ', '.join([self.frequenceToLaTeX(n) for n in note[0]]) + " & " + self.nomDuree(note[1]) + " & \\\\\n"
              result += "\\hline\n"
          else:
            result +=  " &  & \\\\\n"
            result += "\\hline\n"
        if solution:
          result += "\\hline\n"
        result += "\\end{tabular}\n"
        if len(partition) > 30:
          result += "}\n"
        if solution:
          result += "\\caption{Tableau des notes de la partition (solution)}\n"
        else:
          result += "\\caption{Tableau des notes de la partition}\n"
        result += "\\end{table}\n"
      
        return result
      
    def tableauFrequences(self, partitions, beamer = False):
      
      result = ""
      
      notes = self.getOctaveEtIDNoteDansIntervalle(self.minFrequence(partitions), self.maxFrequence(partitions))
      
      if not beamer:
        result += "Fréquences de chacune des notes utilisées sur la partition (arrondies à l'entier le plus proche):"
      
      if beamer:
        result += "{\\footnotesize\n \\begin{center}\n"
      else:
        result += "\\begin{table}[H]\n\\centering\n"
      result += "\\begin{tabular}{|c|c|}\n"
      result += "\\hline\n"
      result += "\\textbf{Fréquence} (Hz) & \\textbf{Note} \\\\ \n"
      result += "\\hline \n"
      
      for note in notes:
        result += str(int(round(self.note(note[0], note[1])))) + " & " + self.toLaTeX(note[0], note[1], 
                                                                                      self.minOctave, self.maxOctave) + "\\\\\n"
        result += "\\hline\n"
      
      result += "\\end{tabular}\n"
      if not beamer:
        result += "\\caption{Table des fréquences de la gamme tempérée}\n"
        result += "\\end{table}\n"
      
      if self.contientSilences(partitions) and not beamer:
        result += "Attention, la partition peut également contenir des silences, dont on peut mesurer la longueur, comme les notes"
        
      if beamer:
        result += "\\end{center}}\n"
      return result
        
    # retourne la translation d'octave nécessaire pour jouer avec
    # les boomwhackers, ou une valeur supérieure à 100 si la note 
    # n'est pas disponible
    def getTranslationOctaveBoomwhackers(self, minNote):
      octaveCible = min([ n[0] for n in self.boomwhackers if n[1] == minNote[1]] + [10000])
      return octaveCible - minNote[0]
    
    def shiftNote(self, note, shiftOctave):
      return (note[0] + shiftOctave, note[1])
    def estJouable(self, note, shiftOctave):
      nN = self.shiftNote(note, shiftOctave)
      return nN in self.boomwhackers
    
    def setShiftOctave(self, partitions):
      notes = [ self.getOctaveEtIDNote(p) for p in self.notesDansPartition(partitions) ]
      
      minNote = min(notes)

      self.shiftOctave = self.getTranslationOctaveBoomwhackers(minNote)
      
      if self.shiftOctave > 100:
        return False
      
      for n in notes:
        if not self.estJouable(n, self.shiftOctave):
          print n, "n'est pas jouable"
          return False
      # we try with a smaller shift
      self.shiftOctave += 1
      for n in notes:
        if not self.estJouable(n, self.shiftOctave):
          self.shiftOctave -= 1
          break
      return True
    
    
    def estDessinable(self, partitions):
      if len(partitions) != 1:
        print "La partition contient plusieurs mains."
        return False
      
      return self.setShiftOctave(partitions)
      
 
    
    def couleurBoomwhackerToTikz(self, mw):
      return "\\definecolor{tempcolor}{rgb}{ "+ \
          str(mw["couleur"][0] / 255.) +  ", " + \
          str(mw["couleur"][1] / 255.) +  ", " + \
          str(mw["couleur"][2] / 255.) +  "}"
        
      #return "{rgb, 255 :red, " + str(mw["couleur"][0]) \
            #+ "; green, " + str(mw["couleur"][1]) \
            #+ "; blue, " + str(mw["couleur"][2]) + "}"
    
    def pointColore(self, boomwhacker, x, y, r):
      result = self.couleurBoomwhackerToTikz(boomwhacker)
      rayon = r * 1.3 if boomwhacker["court"] == "0" else r
      result += "\\fill[fill=tempcolor] ("+ \
                str(x) + "," +  str(y) + ") circle (" + str(rayon) + ");"
      if "alteration" in boomwhacker:
        if (boomwhacker["couleur"][0] + boomwhacker["couleur"][1] + boomwhacker["couleur"][2]) > 128 * 3:
          result += "\\draw (" + str(x) + "," + str(y) + ") node{\\footnotesize " + str(boomwhacker["alteration"]) + "};"
        else:
          result += "\\draw[color=white] (" + str(x) + "," + str(y) + ") node{\\footnotesize " + str(boomwhacker["alteration"]) + "};"
      if boomwhacker["capsule"] == "1":
         result += "\\fill [black] (" + str(x - r/2) + "," + str(y - 1.2 * r) + \
                    ") rectangle (" + str(x + r/2) + ", " + str(y - 1.9 * r) + ");"

      
      return result
    
    def ensembleDesNotesGraphiques(self, frequences, radius, beamer):
      result = ""
      
      notesTotales = sorted(list(set(frequences)))
      
      
      result += "\\begin{table}[H]\centering\n"
      
      if len(notesTotales) > 12:
        n = [ notesTotales[:12],  notesTotales[12:] ]
      else:
        n = [notesTotales]

      for notes in n:
        result += '\\resizebox{1\\textwidth}{!}{%\n'
        if not beamer:
          result += "\\begin{tabular}{|r|" + "".join(["c|"] * len(notes)) +  "}\n"
        else:
          result += "\\begin{tabular}{r|" + "".join(["c"] * len(notes)) +  "}\n"
        if not beamer:
          result += "\\hline\n"
        
        result += "Notes"
        for n in notes:
          result += "&"
          if beamer:
            result += "\\hspace*{.2cm}"
          result += self.frequenceToLaTeX(n, self.minOctave, self.maxOctave)
          if beamer:
            result += "\\hspace*{.2cm}"
        result += "\\\\ \n"
        result += "Forme"
        for n in notes:
          result += "&"
          result += "\\begin{tikzpicture}[]"
          nid = self.shiftNote(self.getOctaveEtIDNote(n), self.shiftOctave)
          mw = self.boomwhackers[nid]
          result += self.pointColore(mw, 0, 0, radius)

          result += "\\end{tikzpicture}"
        result += "\\\\ \n"
        if not beamer:
          result += "Prénom"
          for n in notes:
            result += "&"
            result += "\\hspace*{2cm}"
          result += "\\\\ \n"
        if not beamer:
          result += "\\hline\n"
        result += "\\end{tabular}\n"
        result += "}\n"
        #result += " \\newline \n"
          
      result += "\\caption{Représentation colorée des notes}\n"
      result += "\\end{table}\n"
      return result
    
    def partitionGraphique(self, partitions, beamer = False):


      result = ""
      
      if beamer:
          result += "\\frame{"
          result += "\\frametitle{Représentation graphique}\n"
      
      frequences = self.notesDansPartition(partitions)
      
      x = 0.
      y = 0.
      
      if beamer:
        stepx = 1
        stepy = 1.2
      else:
        stepx = 2
        stepy = 2.5
        
      radius = stepx * 0.18
      
      # if the notes are all short, we increase space between circles
      maxSpace = max(self.dureeEnNoires(note[1]) for note in partitions[0])
      if maxSpace < self.noire:
        stepx *= 2
      print stepx
      
      finDeLigne = self.longueurPartitionColoree * stepx
      
      nbl = 1
      if self.estDessinable(partitions):
        p = partitions[0]

        if beamer:
          result += self.ensembleDesNotesGraphiques(frequences, radius, beamer)
          result += "}\n"
          result += "\\frame{\n"
          result += "\\frametitle{Représentation graphique}\n"
          
          
        result += self.ensembleDesNotesGraphiques(frequences, radius, beamer)

        if not beamer:
          result += "La partition graphique suivante se lit ligne par ligne. Les durées sont représentées par les espaces entre les cercles colorés. \n \n"

        result += "\\begin{figure}[H]"
        result += "\\begin{tikzpicture}[]"
        
        
        for note in p:
          if x == 0:
            result += "\\node[draw] at (- " + str(stepx) + "," + str(y) + ") {" + str(nbl) + "};"
          if isinstance(note[0], float):
            # on dessine le point
            if note[0] != 0:
              n = self.shiftNote(self.getOctaveEtIDNote(note[0]), self.shiftOctave)
              mw = self.boomwhackers[n]
              result += self.pointColore(mw, x, y, radius)
            # on calcule la translation pour le point suivant
            x += self.dureeEnNoires(note[1]) * stepx
            if x >= finDeLigne:
              x = 0
              y -= stepy
              nbl += 1
              
        
        result += "\\end{tikzpicture}"
        if not beamer:
          result += "\\caption{Partition graphique retranscrite depuis le tableau des notes de la partition}"
        result += "\\end{figure}"
      else:
        result += "Cette partition n'est pas représentable sous forme de partition pour boomwhackers diatoniques."
      
      if beamer:
        result += "}\n"
      return result

    def partitionMusicale(self, partitions, beamer = False):
      result = ""
      
      if beamer:
          result += "\\frame{"
          result += "\\frametitle{Partition musicale}"

      if len(partitions) == 1:
        partition = partitions[0]
        
        for n in partition:
          if not isinstance(n[0], float):
            result += "Le générateur d'exercice ne prend pas en charge cette partition."
            return result
        
        notes = [ self.getOctaveEtIDNote(p) for p in self.notesDansPartition(partitions) ]
        minNote = min([ n for n in notes if n[1] > 0])
        (minoctave, minidnote) = minNote
        maxNote = max([ n for n in notes if n[1] > 0])
        (maxoctave, maxidnote) = maxNote
        midOctave = int((maxoctave + minoctave) / 2)
        
        dureePartition = self.longueurPartitionMusicale * self.noire
        longueurCourante = 0
        
        if beamer:
          result += "\\normalmusicsize"
        else:
          result += "\\Largemusicsize"
        result += "\\begin{figure}[H]"
        result += "\\begin{music}"
        #result += "\\parindent10mm"
        #result += "\\generalmeter{\\meterfrac{3}{4}}"
        result += "\\startextract"
        result += "\\notes "
        for i, note in enumerate(partition):
          if note[0] != 0:
            result += self.dureeToMusicTex(note[1]) + "{"
            result += self.frequenceToMusicTeX(note[0], midOctave)
            result += "}\n"
            longueurCourante += note[1]
          else:
            # TODO
            pass
          if longueurCourante >= dureePartition and i < len(partition) - 1:
            longueurCourante = 0
            result +="\\enotes"
            result += "\\zendextract"
            result += "\\end{music}"
            result += "\\begin{music}"
            result += "\\startextract"
            result += "\\notes "
        result +="\\enotes"
        result += "\\zendextract"
        result += "\\end{music}"
        result += "\\caption{Partition simplifiée}"
        result += "\\end{figure}"
      else:
        result += "Le générateur d'exercice ne prend pas en charge cette partition."
      
      if beamer:
        result += "}\n"
      return result


    # cette fonction écrit la fin de l'exercice spectrogramme
    def finExerciceSpectrogramme(self, partitions):
        result = "\n"

        result += "~\\clearpage\n"


        # on écrit le tableau de durées
        result += self.tableauDurees(partitions)


        # on écrit le tableau des fréquences
        result += self.tableauFrequences(partitions)
        

        result += "~\\clearpage\n"
        
        # on écrit le tableau de l'exercice à remplir
        result += "Remplir le tableau par lecture graphique: "
        
        result += self.tableauNotesBrutes(partitions, False)

        return result
      
      
    def finCorrectionSpectrogramme(self, partitions, nomSolution):
        result = "\n"
        result += "~\\clearpage\n"
        result += "\\section{Solution}\n"
        # on écrit le tableau solution de l'exercice
        result += self.tableauNotesBrutes(partitions, True)

        result += "~\\clearpage\n"
        
        result += "\\section{Partition graphique}\n"
        result += self.partitionGraphique(partitions)

        result += "\\section{Partition musicale}\n"
        result += self.partitionMusicale(partitions)

        if nomSolution != "":
          result += "\\section{Solution}\n"
          result += "Cette mélodie s'appelle " + nomSolution
          
        return result

    
    def frequenceEnCm(self, frequence):
      return (self.margeFrequence + frequence - self.fmin) * self.echelleY

    def dureeEnCm(self, duree):
      return duree * self.echelleX
   
    # dessine sur le spectrogramme l'accord donné en paramètre,
    # à l'horodatage donné
    def noteSpectrogrammePartition(self, frequence, duree, horodatage, couleur = "black"):
      if frequence == 0:
        return ''
      else:
        y = self.frequenceEnCm(frequence)
        return self.noteGraphique(y, self.margeDureeDebut, duree, horodatage, couleur)
    
    def noteGraphique(self, y, decalageDuree, duree, horodatage, couleur = "black"):
      xmin = self.dureeEnCm(horodatage + decalageDuree)
      xmilieu = self.dureeEnCm(horodatage + self.dureeFrappe + decalageDuree)
      xmax = self.dureeEnCm(horodatage + duree + decalageDuree)
      result = "\draw[" + couleur +", line width=1.5mm] (" + str(xmin) + "," + str(y) + ") -- (" + str(xmilieu) + "," + str(y) + ");\n"
      result += "\draw[" + couleur +", ultra thick] (" + str(xmilieu) + "," + str(y) + ") -- (" + str(xmax) + "," + str(y) + ");\n"
      return result
    
    # dessine sur le spectrogramme l'accord donné en paramètre,
    # à l'horodatage donné
    def accordSpectrogrammePartition(self, accord, horodatage, couleur):
      if isinstance(accord[0], float):
        return self.noteSpectrogrammePartition(accord[0], accord[1], horodatage, couleur)
      else:
        return ''.join([self.noteSpectrogrammePartition(note, accord[1], horodatage, couleur) for note in accord[0]])
    
    # dessine sur le spectrogramme les notes données en paramètre
    def notesSpectrogrammePartition(self, partition, couleur):
      resultat = ""
      horodatage = 0.
      for note in partition:
        resultat += self.accordSpectrogrammePartition(note, horodatage, couleur)
        horodatage += note[1]
      return resultat
    
    # cette classe dessine les gradations sur les axes des abscisses
    def graduationAbscisses(self):
      nbGraduations = int(self.duree)
      pas = 1.
      while nbGraduations > 10:
        pas *= 2.
        nbGraduations = int(self.duree / pas)
      while nbGraduations < 4:
        pas /= 2.
        nbGraduations = int(self.duree / pas)
      
      
      result = ""
      for i in range(0, nbGraduations + 1):
        h = i * pas
        hinc = self.dureeEnCm(h + self.margeDureeDebut)
        
        result += "\draw[black, thick] (" + str(hinc) + ", -0.1) -- ";
        result += "(" + str(hinc) + ", 0.)\n";
        result += "(" + str(hinc) + ", -0.1)  node[below]{"
        if h.is_integer():
          result += str(int(h))
        else:
          result += '{0:.2f}'.format(h).replace(".", ",")
        result += "};\n";
    
      return result
      
    def graduationOrdonnees(self, partitions):
      frequences = self.notesDansPartition(partitions)


      result = ""
      
      for frequence in frequences:
        finc = self.frequenceEnCm(frequence)
        result += "\draw[black, thick] (-0.1, " + str(finc) + ") -- ";
        result += "(0," + str(finc) + ")\n";
        result += "(-0.1," + str(finc) + ")  node[left]{" + str(int(round(frequence))) + "};\n";
    
      return result
    
    def lignesVerticales(self, premier = False):
      result = ""
      
      #espace = self.doublecroche
      espace = self.noire
      nbsteps = int(self.duree / espace) + 1
      if nbsteps > 100:
        espace = self.croche
        nbsteps = int(self.duree / espace) + 1
        if nbsteps > 100:
          espace = self.noire
          nbsteps = int(self.duree / espace) + 1
        
      
      for i in range(0, nbsteps):
        if i != 0 or premier:
          h = i * espace
          hinc = self.dureeEnCm(h + self.margeDureeDebut)
          
          if i % 4 == 0:
            result += "\draw[thin] (" + str(hinc) + ", 0) -- ";
          else:
            result += "\draw[ultra thin] (" + str(hinc) + ", 0) -- ";
          result += "(" + str(hinc) + ", " + str(self.hauteur) + ");\n";
      return result
    
    def lignesHorizontales(self, partitions):

      result = ""
      
      frequences = self.notesDansIntervalle(self.minFrequence(partitions), self.maxFrequence(partitions), True)
      for frequence in frequences:
        finc = self.frequenceEnCm(frequence)
        result += "\draw[thin] (0, " + str(finc) + ") -- ";
        result += "(" + str(self.largeur) + ", " + str(finc) + ");\n";
    
      frequences = self.notesDansIntervalle(self.minFrequence(partitions), self.maxFrequence(partitions))
      for frequence in frequences:
        finc = self.frequenceEnCm(frequence)
        result += "\draw[dashed, ultra thin] (0, " + str(finc) + ") -- ";
        result += "(" + str(self.largeur) + ", " + str(finc) + ");\n";

      return result
    
    # cette fonction écrit le spectrogramme simplifié correspondant à la 
    # partition donnée en paramètre
    def spectrogramme(self, partitions, beamer = False):
        result = ""

        # on commence par initialiser l'échelle
        if beamer:
          result += "\\frame{"
          result += "\\frametitle{Spectrogramme}"
          self.initialisationEchelleSpectrogramme(partitions, 12 - 2 * self.margeDocument - self.margeLegende, 7)
        else:
          self.initialisationEchelleSpectrogramme(partitions, 21 - 2 * self.margeDocument - self.margeLegende, 12)
          
          

        result += "\\begin{tikzpicture}[]"
        
        # lignes verticales
        result += self.lignesVerticales()
        # lignes horizontales
        result += self.lignesHorizontales(partitions)


        # axe des abscisses
        result += "\draw[->, black, thick] (0, 0) -- ";
        result += "(" + str(self.largeur) + ", 0) node[below]{temps (s)};\n";

        # axe des ordonnées
        result += "\draw[black, dashed, thick] (0, 0) -- ";
        result += "(0," + str(self.frequenceEnCm(self.fmin)) + ");\n";

        result += "\draw[->, black, thick] (0, " + str(self.frequenceEnCm(self.fmin)) + ") -- ";
        result += "(0," + str(self.hauteur) + ")  node[above]{fréquence (Hz)};\n";
        
        # gradations sur les axes
        result += self.graduationAbscisses()
        result += self.graduationOrdonnees(partitions)
                
        couleur = { 0: "black", 1: "darkgray", 2: "gray"}
        for i,p in enumerate(partitions):
          result += self.notesSpectrogrammePartition(p, couleur.get(i, "lightgray"))

        result += "\\end{tikzpicture}"
        
        if beamer:
          result += "}\n"
        return result

  
    # Genère une feuille d'exercice au format LaTeX à partir des notes donnees 
    # en paramètre.
    # Paramètres:
    # * partitions: une liste de listes de notes sous la forme  [ [ [ freq1, duree1], [ freq2, duree2], ... ],
    #                                                           [ [ freq1, duree1], [ freq2, duree2], ... ] ]
    #   où chaque liste décrit la partition d'une "main". Une partition est décrite comme 
    #   une liste d'éléments composés d'une ou plusieurs notes (quantifiée par une frequence) et d'une duree exprimee en seconde.
    # * nomFichier: le nom d'un fichier où sera sauvée la feuille d'exercice
    def genererExerciceSpectrogramme(self, partitions, nomFichier, nomMelodie):
       
        self.setMinMaxOctaves(partitions)
       
        # on ouvre le fichier
        l_fichier = open(nomFichier, "w")
        
        # on écrit les entêtes du document LaTeX
        l_fichier.write(self.enteteLaTex(nomMelodie))
        
        # on écrit le début de l'exercice
        l_fichier.write(self.debutExerciceSpectrogramme())
        
        # on écrit le spectrogramme
        l_fichier.write(self.spectrogramme(partitions))
        
        # on écrit la suite de l'exercice
        l_fichier.write(self.finExerciceSpectrogramme(partitions))
        
        # on écrit la fin du document LaTeX
        l_fichier.write(self.piedLaTeX())
        
        # on ferme le fichier
        l_fichier.close()

    # Genère une feuille de correction de l'exercice, au format LaTeX 
    # à partir des notes donnees en paramètre.
    def genererCorrectionSpectrogramme(self, partitions, nomFichier, nomMelodie, nomSolution=""):
        self.setMinMaxOctaves(partitions)
        
        # on ouvre le fichier
        l_fichier = open(nomFichier, "w")
        
        # on écrit les entêtes du document LaTeX
        l_fichier.write(self.enteteLaTex("Correction de " + nomMelodie))
        
        # on écrit le début de l'exercice
        l_fichier.write(self.debutExerciceSpectrogramme(False))
        
        # on écrit la suite de l'exercice
        l_fichier.write(self.finCorrectionSpectrogramme(partitions, nomSolution))
        
        # on écrit la fin du document LaTeX
        l_fichier.write(self.piedLaTeX())
        
        # on ferme le fichier
        l_fichier.close()
        
    def genererTransparentSpectrogramme(self, partitions, nomFichier, nomMelodie, nomSolution=""):
        self.setMinMaxOctaves(partitions)
        # on ouvre le fichier
        l_fichier = open(nomFichier, "w")
        
        # on écrit les entêtes du document LaTeX
        l_fichier.write(self.enteteBeamer(nomMelodie))
        
        l_fichier.write(self.spectrogramme(partitions, True))
        
        l_fichier.write("\\frame{\n")
        l_fichier.write("\\frametitle{Durées et notes}\n")
        # on écrit le tableau de durées
        l_fichier.write(self.tableauDurees(partitions, True))
        # on écrit le tableau des fréquences
        l_fichier.write(self.tableauFrequences(partitions, True))
        l_fichier.write("}\n")
        
        
        l_fichier.write(self.partitionGraphique(partitions, True))

        l_fichier.write(self.partitionMusicale(partitions, True))

        if nomSolution != "":
          result = "\\frame{\n"
          result += "\\frametitle{Solution}\n"
          result += "\\begin{center}\Large Cette mélodie s'appelle " + nomSolution + "\\end{center}"
          result += "}\n"
          l_fichier.write(result)


        
        # on écrit la fin du document LaTeX
        l_fichier.write(self.piedBeamer())
        
        # on ferme le fichier
        l_fichier.close()        
