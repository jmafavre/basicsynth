# coding: utf8
# -*- coding: utf-8 -*-
import wave
import math
import binascii
import copy
import sys
from itertools import repeat
import tempfile
import os
import pickle

from gamme import GammeTemperee

# (C) Pascal Lafourcade, Jean-Marie Favreau

# base sur une idee originale de Fabrice Sincere et Jean-Claude Meilland
## http://fsincere.free.fr/isn/python/cours_python_ch9.php


# la classe MagasinDEchantillons est un outil permettant de stocker des échantillons,
# en vue de réutiliser des valeurs pré-calculées
class MagasinDEchantillons(object):

  def __init__(self, frequenceEchantillonnage):
    self.echantillon = {}
    self.frequenceEchantillonnage = frequenceEchantillonnage

  
  # retourne vrai si l'échantillon existe
  def existeEchantillon(self, cleTableau, idEchantillon):
    if not cleTableau in self.echantillon:
      return False
    else:
      return len(self.echantillon[cleTableau]) > idEchantillon
    
  # retourne vrai si la série d'échantillons existe
  def existeSerieEchantillons(self, cleTableau):
    return cleTableau in self.echantillon

  # génère un nom de fichier de cache
  def nomFichierCache(self, cleTableau):
    
    return  os.path.join(tempfile.gettempdir(), "son-" + str(cleTableau) + ".cache")

  # étant donné une fréquence, charge depuis le fichier correspondant
  # le son déjà calculé
  def chargerDepuisCache(self, cleTableau):
    fn = self.nomFichierCache(cleTableau)
    if os.path.isfile(fn):
      try:
        with open(fn, 'rb') as f:
          self.echantillon[cleTableau] = pickle.load(f)
        return self.echantillon[cleTableau]
      except EOFError:
        return None
    return None

  # sauve le son déjà calculé pour cette fréquence depuis un fichier
  def sauverDansCache(self, cleTableau):
    fn = self.nomFichierCache(cleTableau)
    with open(fn, 'wb') as f:
      pickle.dump(self.echantillon[cleTableau], f)


# la classe MagasinDEnveloppe stocke les échantillons nécessaires
# à la description d'une enveloppe.
class MagasinDEnveloppe(MagasinDEchantillons):
  def __init__(self, frequenceEchantillonnage):
      super(MagasinDEnveloppe, self).__init__(frequenceEchantillonnage)
  
  def getValeurs(self, cleTableau):
    return self.echantillon[cleTableau]

  def ranger(self, cleTableau, tableau):
    self.echantillon[cleTableau] = tableau
    self.sauverDansCache(cleTableau)

# la classe enveloppe donne le moyen de moduler l'amplitude (le volume) du son en fonction
# du temps. Habituellement, on decompose un son musical en 4 parties: l'attaque, le declin,
# le maintien, et la relâche.
#
#   Amplitude (= niveau)
#     ^
# A_M |        /\
#     |       /  \
#     |      /    \
#     |     /      \_______________________________
# A_m |    /                                       \
#     |   /                                         \
#     |  /                                           \
#     | /                                             \
#     +--------------------------------------------------------> temps
#      [  A  | D  |                S               | R ]
#  
#  A: Attack (temps d'attaque)
#  D: Decay (temps de declin)
#  S: Sustain (temps de maintien)
#  R: Release (temps de relâchement)
#  
#  A_M: amplitude maximale
#  A_m: amplitude de maintien
class Enveloppe:
    magasin = None

    # constructeur de la classe, qui prend en parametre les 4 durees de la note 
    # Parametres:
    # * attaque: duree de l'attaque en seconde
    # * declin: duree du declin en seconde
    # * maintien: duree du maintien en seconde
    # * relache: duree de la relache en seconde
    # * fech: frequence d'echantillonnage
    # * amplitudeMax: amplitude du son maximale, a la fin de l'attaque
    # * amplitudeMaintien: amplitude du son lors du maintien
    def __init__(self, attaque, declin, maintien, relache, fech, amplitudeMax = 1.0, amplitudeMaintien = 0.1):
      self.dureeAttaque = attaque
      self.dureeDeclin = declin
      self.dureeMaintien = maintien
      self.dureeRelache = relache
      self.amplitudeMax = amplitudeMax
      self.amplitudeMaintien = amplitudeMaintien
      self.fech = fech
      
      if self.dureeDeclin != 0:
        self.coefDeclin = (self.amplitudeMax - self.amplitudeMaintien) / (self.dureeDeclin) 
      else:
        self.coefDeclin = 0
      if self.dureeRelache != 0:
        self.coefRelache = self.amplitudeMaintien / self.dureeRelache
      else:
        self.coefRelache = 0
      
      # on calcule les elements de temps
      self.duree = self.dureeAttaque + self.dureeDeclin + self.dureeMaintien + self.dureeRelache
      self.debutDeclin = self.dureeAttaque
      self.debutMaintien = self.dureeAttaque + self.dureeDeclin
      self.debutRelache = self.dureeAttaque + self.dureeDeclin + self.dureeMaintien
      
      self.genererEnveloppe()
    
    def getCle(self):
      return "ech" + str(hash((self.dureeAttaque, self.dureeDeclin, self.dureeMaintien, self.dureeRelache, self.amplitudeMax, self.amplitudeMaintien, self.fech)))
    
    def genererEnveloppe(self):
      # si le magasin n'existe pas, on le créé
      if Enveloppe.magasin is None:
        Enveloppe.magasin = MagasinDEnveloppe(self.fech)
      cle = self.getCle()
      
      # si l'enveloppe n'existe pas, on la charge
      if not Enveloppe.magasin.existeSerieEchantillons(cle):
        self.valeurs = Enveloppe.magasin.chargerDepuisCache(cle)
        # si elle n'est pas chargeable, on la créé, et on la range dans le magasin
        if not Enveloppe.magasin.existeSerieEchantillons(cle):
          self.valeurs = [ self.calculerValeur(i) for i in range(0, int((self.duree) * self.fech))]
          Enveloppe.magasin.ranger(cle, self.valeurs)
      else:
        self.valeurs = Enveloppe.magasin.getValeurs(cle)
  
          
    # methode statique permettant de construire une enveloppe simple, avec une duree d'attaque et declin
    # identique (correspondant à la frappe d'une corde), un long maintien, et une courte relache
    # Parametres:
    # * dureeEnSeconde: duree totale du son, en seconde
    # * dureeFrappe: duree cumulee de l'attaque et du declin
    @staticmethod
    def enveloppeAvecFrappeSimple(dureeEnSeconde, dureeFrappe, fech):
        att = dureeFrappe / 2
        if dureeEnSeconde < 4 * att:
          dureeEnSeconde = 4 * att
        return Enveloppe(att, att, dureeEnSeconde - 3 * att, att, fech)

    # methode statique permettant de construire une enveloppe simple sans frappe, avec une duree de declin
    # et de maintien nulle, une duree d'attaque très courte, et une longue relâche
    # Paramètres:
    # * dureeEnSeconde: duree totale du son, en seconde
    # * dureeAttaque: duree de l'attaque
    # * maxAmplitude: amplitude au maximum
    @staticmethod
    def enveloppeSansFrappeSimple(dureeEnSeconde, dureeAttaque, fech, maxAmplitude = 1.0):
        return Enveloppe(dureeAttaque, 0.0, 0.0, dureeEnSeconde - dureeAttaque, fech, maxAmplitude, maxAmplitude)

    # methode statique permettant de construire une enveloppe simple sans frappe, sous la forme 0-1 : passe instantanément
    # d'un son nul à un son au volume maximum, et avec une sortie aussi brutale.
    # Paramètres:
    # * dureeEnSeconde: duree totale du son, en seconde
    # * maxAmplitude: amplitude au maximum
    @staticmethod
    def enveloppeZeroUn(dureeEnSeconde, fech, maxAmplitude = 1.0):
        return Enveloppe(0.0, 0.0, dureeEnSeconde, 0.0, fech, maxAmplitude, maxAmplitude)

   
    # methode statique permettant de construire une 'enveloppe sous forme d'une montée
    # progressive, puis d'une descente: "/\"
    # Paramètres:
    # * dureeEnSeconde: duree du son produit par le synthetiseur lorsqu'une note est jouee
    @staticmethod
    def enveloppeMonteeDescente(dureeEnSeconde, fech, maxAmplitude = 1.0):
        return Enveloppe(dureeEnSeconde / 2, 0.0, 0.0, dureeEnSeconde / 2, fech, maxAmplitude, maxAmplitude)


   
    # methode permettant de modifier la duree de la note, en reduisant le maintien pour 
    # qu'il corresponde a la contrainte donnee
    # Parametres:
    # * dureeAvantRelache: duree maximum (en seconde) avant la relache
    def getEnveloppeReduite(self, dureeAvantRelache):
      attaque = self.dureeAttaque
      declin = self.dureeDeclin
      maintien = self.dureeMaintien
      relache = self.dureeRelache
      amplitudeMaintien = self.amplitudeMaintien
      if dureeAvantRelache < attaque + declin + maintien:
          nouveauMaintien = dureeAvantRelache - (attaque + declin)
          if nouveauMaintien >= 0:
            maintien = nouveauMaintien
          else:
            maintien = 0.
            relache = 0.
            amplitudeMaintien = 0.
            
      return Enveloppe(attaque, declin, maintien, relache, self.fech, self.amplitudeMax, amplitudeMaintien)
  
    # calcule la valeur d'amplitude de l'enveloppe a un temps donne. Attention: la fonction
    # peut retourner une valeur négative, qu'il faudra ignorer
    # Parametre:
    # * idEch: échantillon ou sera calculee la valeur de l'enveloppe 
    def calculerValeur(self, idEch):
      temps = float(idEch) / self.fech
      
      if temps < self.debutMaintien:
        if temps < self.debutDeclin:
          return temps / self.dureeAttaque
        else:
          return (self.amplitudeMaintien + (self.debutMaintien - temps) * self.coefDeclin)
      else:
        if temps < self.debutRelache:
          return (self.amplitudeMaintien)
        else:
          if temps < self.duree:
            return (self.duree - temps) * self.coefRelache
          else:
            return 0.
    
    # retourne ma valeur de l'échantillon
    def valeur(self, idEch):
      if idEch >= len(self.valeurs):
        return 0.
      else:
        return self.valeurs[idEch]

    # modidifieur de la duree d'attaque
    def setDureeAttaque(self, duree):
      self.dureeAttaque = duree
			
    # modidifieur de la duree de declin
    def setDureeDeclin(self, duree):
      self.dureeDeclin = duree
		
    # modidifieur de la duree de maintien
    def setDureeMaintien(self, duree):
      self.dureeMaintien = duree

    # modidifieur de la duree de relâche
    def setDureeRelache(self, duree):
      self.dureeRelache = duree



# la classe GenerateurDeNote est un outil permettant de générer du son, et de le stocker, en vue de 
# réutiliser des valeurs pré-calculées de sons
class GenerateurDeNote(MagasinDEchantillons):

  def __init__(self, frequenceEchantillonnage):
    super(GenerateurDeNote, self).__init__(frequenceEchantillonnage)
  
  # cette méthode permet de vérifier que la note souhaitée, à la fréquence d'échantillonnage donnée, et pour 
  # le nombre d'échantillonnage indiqué est présente en mémoire.
  # Si elle n'est pas présente, on la charge depuis un fichier temporaire. Si aucun fichier temporaire n'existe,
  # on créé les échantillons, et on les sauve sur le disque
  def prepare(self, frequenceNote, nbEchantillons, etendre = False):
    
    # si l'échantillon n'est pas disponible
    if not self.existeEchantillon(frequenceNote, nbEchantillons):
      # on commence par charger depuis le cache fichier
      self.chargerDepuisCache(frequenceNote)

      
      # si cette fréquence n'a jamais été générée, on commence par créer sa liste
      if not frequenceNote in self.echantillon:
        self.echantillon[frequenceNote] = []

      # on calcule le nombre d'échantillons déjà calculés pour cette fréquence
      echExistants = len(self.echantillon[frequenceNote])
        
      # on calcule la borne de calcul, soit comme le nombre d'échantillons souhaités,
      # soit en doublant la taille existante (croissance exponentielle de la taille, pour éviter
      # de tout le temps rajouter des échantillons)
      borne = nbEchantillons
      if etendre and echExistants != 0:
        nouvelleBorne = echExistants * 2
        if nouvelleBorne > borne:
          borne = nouvelleBorne
          
      nouveauxEchantillons = [math.sin(2.0 * math.pi * frequenceNote * k / self.frequenceEchantillonnage)  for k in range(echExistants, borne + 1)]
                                                                                                                          
      self.echantillon[frequenceNote] = self.echantillon[frequenceNote] + nouveauxEchantillons
      
      self.sauverDansCache(frequenceNote)
      
        
  
  # cette méthode retourne pour l'échantillon indiqué (idEchantillon) la valeur de la note donnée,
  # en considérant l'échantillonnage.
  # Si cette information n'est pas en mémoire, la préparation de la note est lancée pour un certain nombre d'échantillons.
  def getEchantillon(self, frequenceNote, idEchantillon):
    
    # si l'échantillon n'a pas déjà été calculé, on précalcule les échantillons
    if not self.existeEchantillon(frequenceNote, idEchantillon):
      self.prepare(frequenceNote, idEchantillon, True)
    
    return self.echantillon[frequenceNote][idEchantillon]
    
    

# la class BasicNote decrit la synthèse d'une note, definie par une frequence, une enveloppe, ainsi qu'une duree.
# La note peut egalement être augmentee d'harmoniques
class BasicNote:

    delaiEntreTouches = 0.02 # le delai de relâche entre deux notes sur le clavier, utilisee pour ajuster les enveloppes

    # constructeur d'une note. Le temps courrant est initialise) au debut.
    # Paramètres:
    # * generateurDeNote: un objet GenerateurDeNote, qui stocke les sons déjà générés
    # * enveloppe: un objet Enveloppe, qui decrit l'evolution de l'amplitude de la note à travers le temps
    # * fech: la frequence d'echantillonnage, ie le nombre d'echantillons par seconde. Une valeur classique est 44100
    # * frequence: la frequence de la note, correspondant à sa hauteur
    # * duree: la duree de la note, correspondant non pas à la duree du son, mais à sa duree sur la partition (ie noire, blanche, etc.)
    # * nbHarmoniques: nombre d'harmoniques qui viendront enrichir la note
    def __init__(self, generateurDeNote, enveloppe, fech, frequence, duree, nbHarmoniques = 0, dureeFrappe = 0.04):
        self.generateurDeNote = generateurDeNote
        self.getEch = self.generateurDeNote.getEchantillon
        
        self.frequence = frequence
        self.duree = duree
        self.enveloppe = enveloppe.getEnveloppeReduite(duree - self.delaiEntreTouches)
        self.fech = fech
        
        self.setMomentCourant()

        
        self.nbEchantillonsSon = int((enveloppe.duree) * fech)
        self.nbEchantillonsNote = int(self.duree * self.fech)

        
        # on construit l'enveloppe des harmoniques sans avoir une attaque + decay de très grande amplitude
        self.nbHarmoniques = nbHarmoniques
        if nbHarmoniques != 0:
          self.enveloppeHarmoniques = Enveloppe.enveloppeSansFrappeSimple(self.enveloppe.duree, dureeFrappe, self.fech)
          self.frequHarmonique = []
          self.coefHarmonique = []
          for h in range(0, self.nbHarmoniques):
            # on calcule la frequence de l'harmonique h
            self.frequHarmonique.append(self.frequence * (h + 2))
            self.coefHarmonique.append(0.5 ** (h + 8))

        self.precalculerSons()
        
    # on appelle le générateur de notes afin qu'il prépare les valeurs du son pour toute la durée de la note,
    # et pour chacune des harmoniques
    def precalculerSons(self):
        # on pré-génère les sons sur toute la note
        self.generateurDeNote.prepare(self.frequence, self.nbEchantillonsSon)
      
        if self.nbHarmoniques != 0:
          # on pré-génère les sons des harmoniques sur toute la note
          for h in range(0, self.nbHarmoniques):
            # on calcule la frequence de l'harmonique h
            frequHarmonique = self.frequence * (h + 2)        
            self.generateurDeNote.prepare(frequHarmonique, self.nbEchantillonsSon)
        
    def setMomentCourant(self, valeur = 0):
      self.courant = valeur

    # retourne la valeur d'amplitude courante en utilisant l'enveloppe, le moment courant,
    # et le niveau donne en paramètre
    # Paramètre:
    # * niveau: le niveau sonore pour jouer la note
    def getAmplitudeCourante(self):
      return self.enveloppe.valeur(self.courant)
    
    def getAmplitudeHarmoniqueCourante(self):
      return self.enveloppeHarmoniques.valeur(self.courant)

    # retourne vrai si la note est finie (mais pas necessairement le son produit par le note, car le son peut depasser
    # la duree de la note donnee sur la partition)
    def estFinieNote(self):
      return self.courant > self.nbEchantillonsNote


    # retourne le nombre d'echantillons qui restent à jouer
    def getNombreEchantillonsRestant(self):
      return self.nbEchantillonsSon - self.courant
    
    # retourne vrai si la note et le son qu'elle a produit sont finis
    def estFiniSonEtNote(self):
      return self.courant >= self.nbEchantillonsSon and self.estFinieNote()
    
    # retourne les amplitudes des echantillons au moment courant, et fait avancer
    # ce moment courant
    def getAmplitudesSuivante(self, niveaux):
      
      
      # on calcule l'amplitude courante grâce à l'enveloppe
      facteur = self.getAmplitudeCourante()
      # on calcule la valeur grâce à la frequence, seulement si l'amplitude n'est pas nulle
      if facteur > 0.:
        facteur = facteur * self.getEch(self.frequence, self.courant)
      else:
        facteur = 0.

      # on ajuste pour chaque canal la valeur de la note suivant le niveau choisi
      values = map(lambda x: x * facteur, niveaux)
 
      if self.nbHarmoniques != 0:
        # on ajoute les harmoniques
        hfacteur = self.getAmplitudeHarmoniqueCourante()
        if hfacteur > 0:
          for h, frequHarmonique in enumerate(self.frequHarmonique):
          
            # on determine son amplitude grâce à l'enveloppe
            # on calcule la valeur de l'harmonique grâce à la frequence, seulement si l'amplitude n'est pas nulle
            if hfacteur > 0.:
              facteur = hfacteur * self.getEch(frequHarmonique, self.courant) * self.coefHarmonique[h]
            else:
              facteur = 0.
            
            # on ajoute l'harmonique à la valeur
            values = map(lambda x, y: x + y * facteur, values, niveaux)
      
      # on passe à l'echantillon suivant
      self.courant = self.courant + 1
  
      return values


# la classe Partition contient une serie de notes, donnees sous la forme [ [ freq1, duree1], [ freq2, duree2], ... ]
# c'est-à-dire comme une liste d'éléments composés d'une ou plusieurs notes (quantifiée par une frequence) et d'une duree exprimee en seconde.
# En utilisant la fonction getNoteSuivante, on obtient à chaque appel une nouvelle note de la partition, ou None si on arrive à la fin de la partition.
class Partition:
  
  def __init__(self, notes):
    self.notes = notes
    self.idNote = 0
    
  def getNoteSuivante(self):
    if self.idNote >= len(self.notes):
      return None
    else:
      result = self.notes[self.idNote]
      self.idNote = self.idNote + 1
      return result
      



# La class BasicSequenceur decrit un sequenceur très simple, utilisant les notes 
# de la gamme chromatique.
# Ce synthetiseur imite un instrument à touches (type piano), où chaque touche
# frappee produirait un son d'une duree fixe. Ce son peut être decrit par une 
# enveloppe pour moduler son amplitude au fil du temps.
class BasicSequenceur(GammeTemperee):

    nbOctet = 4    # taille d'un echantillon : 1 octet = 8 bits. Possible values: 1, 2 or 4
    fech = 44100   # frequence d'echantillonnage: nombre d'echantillon par seconde

    
    # constructeur par defaut de la classe.
    # Paramètres:
    # * nbCanaux: nombre de canaux (1: mono, 2: stereo, ...)
    # * bpm: nombre de battements par minute (= nombre de noires par minute)
    # * nbHarmoniques: nombre d'harmoniques composant le son
    # * dureeSon: duree maximale d'un son (enveloppe complète).
    def __init__(self, nbCanaux = 1, bpm = 120, nbHarmoniques = 0, dureeSon = 4):
        GammeTemperee.__init__(self, bpm)
        self.generateurDeNote = GenerateurDeNote(self.fech)
        self.nbHarmoniques = nbHarmoniques
        self.setEnveloppeFrappeSimple(dureeSon)
        self.setNbCanaux(nbCanaux)

    # Permet d'ajuster l'enveloppe d'un son produit par le synthetiseur, sans aucune attaque, relâche...
    # le son passe d'un volume nul à un volume maximal instantanément, et inversement à la fin.
    # Paramètres:
    # * dureeEnSeconde: duree maximale du son produit par le synthetiseur lorsqu'une note est jouee
    def setEnveloppeZeroUn(self, dureeEnSeconde = 4):
        self.dureeAttaqueEnveloppe = 0
        self.enveloppe = Enveloppe.enveloppeZeroUn(dureeEnSeconde, self.fech)

    # Permet d'ajuster l'enveloppe d'un son produit par le synthetiseur, sous forme d'une montée
    # progressive, puis d'une descente: "/\"
    # Paramètres:
    # * dureeEnSeconde: duree maximale du son produit par le synthetiseur lorsqu'une note est jouee
    def setEnveloppeMonteeDescente(self, dureeEnSeconde = 4):
        self.dureeAttaqueEnveloppe = 0
        self.enveloppe = Enveloppe.enveloppeMonteeDescente(dureeEnSeconde, self.fech)


    # Permet d'ajuster l'enveloppe d'un son produit par le synthetiseur, en imitant un instrument
    # percussif, comme celui d'un piano.
    # Paramètres:
    # * dureeEnSeconde: duree maximale du son produit par le synthetiseur lorsqu'une note est jouee
    # * dureeFrappe: duree de la frappe (première partie du son, correspondant dans l'enveloppe
    #   à l'attaque et au declin).
    def setEnveloppeFrappeSimple(self, dureeEnSeconde = 4, dureeFrappe = 0.02):
        self.dureeAttaqueEnveloppe = dureeFrappe / 4
        self.enveloppe = Enveloppe.enveloppeAvecFrappeSimple(dureeEnSeconde, dureeFrappe, self.fech)



    # ajustement du volume du morceau
    # Paramètre:
    # * niveau: une valeur comprise entre 0 et 1 pour ajuster le volume du son le plus fort genere par 
    #   le synthetiseur
    def setVolume(self, niveau):
      self.niveaux = [ niveau for x in range(0, self.nbCanal) ]

    # Ajustement du nombre de canaux dans le son genere
    # Paramètre: 
    # * nbCanaux: nombre de canaux (1: mono, 2: stereo, ...)
    def setNbCanaux(self, nb):
      self.nbCanal = nb
      self.niveaux = [ 1. for x in range(0, nb) ]
    
    
    # Construit une nouvelle note, d'une frequence et d'une duree donnee, en utilisant l'enveloppe courante du synthetiseur
    # Paramètres:
    # * frequence: la frequence de la note à jouer
    # * duree: la duree de la note à jouer, exprimee en seconde
    def nouvelleNote(self, frequence, duree):
      return BasicNote(self.generateurDeNote, self.enveloppe, self.fech, frequence, duree, self.nbHarmoniques, self.dureeAttaqueEnveloppe)
    
    # Retourne vrai si la valeur donnee est comprise dans l'intervalle des valeurs representant un son.
    def dansIntervalle(self, value, maxVol):
      if value < -maxVol:
        return -maxVol
      elif value > maxVol:
        return maxVol
      else:
        return value
      
    # Cette méthode ajoute à la melodie en cours un echantillon de son supplementaire, 
    # en utilisant la liste des sons qui sont actuellement en train d'être jouees
    def genererEchantillon(self):

      # puis pour chacun des canaux, on calcule l'amplitude suivante
      values = [ 0. for x in range(0, self.nbCanal) ]
      valD = 0.
      for son in self.sons:
        amplitudes = son.getAmplitudesSuivante(self.niveaux)
        
        values = map(lambda x, y: x + y, values, amplitudes)

      # on met à jour la valeur maximale, afin de normaliser le son 
      # à la fin de la generation
      curMax = max(values)
      if curMax > self.maxVal:
        self.maxVal = curMax
      
      # puis on ajoute ces valeurs à la melodie en cours de construction
      self.melodie.append(values)
      
    # retourne le nombre maximum d'echantillons restant dans les sons à jouer
    def getNombreEchantillonsRestant(self):
      resultat = 0
      for son in self.sons:
        restantSon = son.getNombreEchantillonsRestant()
        if restantSon > resultat:
          resultat = restantSon
      return resultat

    # methode interne qui parcourt toutes les partitions et ajoute
    # au besoin les notes qui vont devoir etre jouees
    def echantillonSuivant(self):
      # pour chacune des partitions, si un compteur est à zéro, on 
        # ajoute la note suivante
        for index, cpt in enumerate(self.compteur):
          if cpt == 0:
            notes = self.mains[index].getNoteSuivante()
            if notes == None:
              cpt = -1
            else:
              if isinstance(notes[0], list):
                # on cree une note par note de l'accord
                for n in notes[0]:
                  self.sons.append(self.nouvelleNote(n, notes[1]))
              else:
                # on cree la nouvelle note
                self.sons.append(self.nouvelleNote(notes[0], notes[1]))
        
              # on calcule le nombre d'echantillons necessaire à la note
              self.compteur[index] = self.sons[-1].nbEchantillonsNote

              print('Veuillez patienter...')
          else:
            if cpt > 0:
              self.compteur[index] = cpt - 1

    # Cree une melodie à à partir de la liste des notes et durees donnee en paramètre.
    # Le resultat sera stocke sous forme d'un echantillonnage dans la variable self.melodie
    # Paramètre:1
    # * partitions: une liste de listes de notes sous la forme  [ [ [ freq1, duree1], [ freq2, duree2], ... ],
    #                                                           [ [ freq1, duree1], [ freq2, duree2], ... ] ]
    #   où chaque liste décrit la partition d'une "main". Une partition est décrite comme 
    #   une liste d'éléments composés d'une ou plusieurs notes (quantifiée par une frequence) et d'une duree exprimee en seconde.
    def creerMelodie(self, partitions):
      self.melodie = []
      self.maxVal = 0.
      
      genEch = self.genererEchantillon # pour accelerer le calcul, on resout en avance la recherche de la methode
      echSuivant = self.echantillonSuivant
    
      print ("Creation de la melodie")
      
      # creation des objets partition qui contiennent les séries de notes, et des compteurs de fin de note
      self.mains = [ Partition(p) for p in partitions]
      self.compteur = [ 0 for p in partitions]

      self.sons = []
      
      echSuivant()
      
      while self.sons:
        
        # on genère les echantillons de la note
        genEch()

        echSuivant()
          
        # on filtre ensuite les sons en ne gardant que ceux qui ne sont pas encore finis
        self.sons = [ x for x in self.sons if not x.estFiniSonEtNote() ]

      
      
    # Sauve dans un fichier wave la melodie actuellement contenue en memoire
    # Paramètre:
    # * nomFichier: le nom d'un fichier où sera sauvee la melodie, au format wave
    def sauverMelodie(self, nomFichier):
      
      print ("Export en wave de la melodie")
      
      fichierSon = wave.open(nomFichier,'w') # instanciation de l'objet fichierSon
      
      nbEchantillon = len(self.melodie)
      
      parametres = (self.nbCanal, self.nbOctet, self.fech, nbEchantillon, 'NONE','not compressed')
      fichierSon.setparams(parametres)    # creation de l'en-tete (44 octets)

      # pour chacun des echantillons de la melodie
      for values in self.melodie:
        # on produit pour chaque canal sa valeur
        for i in range(0, len(values)):
          niveau = self.niveaux[i]
          value = values[i]
          # et on l'écrit dans le fichier
          packWrite = { 1: 'b', 2: 'h', 4: 'i' }
          maxVol = 2**(8 * self.nbOctet - 1) -1.0
          fichierSon.writeframesraw(wave.struct.pack(packWrite[self.nbOctet], int(self.dansIntervalle(niveau * maxVol * value / self.maxVal, maxVol))))

      print('Export dans le fichier "' + nomFichier + '"')
      # on ferme le fichier pour qu'il soit bien exporte
      fichierSon.close()

    # Genère une melodie à partir des notes donnees en paramètre, puis la sauve dans un fichier wave
    # Paramètres:
    # * partitions: une liste de listes de notes sous la forme  [ [ [ freq1, duree1], [ freq2, duree2], ... ],
    #                                                           [ [ freq1, duree1], [ freq2, duree2], ... ] ]
    #   où chaque liste décrit la partition d'une "main". Une partition est décrite comme 
    #   une liste d'éléments composés d'une ou plusieurs notes (quantifiée par une frequence) et d'une duree exprimee en seconde.
    # * nomFichier: le nom d'un fichier où sera sauvee la melodie, au format wave
    def genererMelodie(self, partitions, nomFichier):
      
        # creation de la melodie
        self.creerMelodie(partitions)
        
        # sauvegarde dans un fichier wave
        self.sauverMelodie(nomFichier)
        

        


    
