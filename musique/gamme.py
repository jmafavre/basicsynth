# coding: utf8
# -*- coding: utf-8 -*-

# la classe GammeTemperee décrit les notes de la gamme dite tempérée. Il s'agit du système d'accord
# utilisé par les pianos, qui divise l'octave en intervalles chromatiques égaux.
# Cette classe décrit également les durées telles qu'elles sont notées sur les partitions (noires, 
# blanches, etc), suivant un battement par minutes donné.
class GammeTemperee:

    def __init__(self, bpm = 120):
        self.frequences = [4186.01, 4434.92, 4698.64, 4978.03, 5274.04, 5587.65, 5919.91, 6271.93, 6644.88, 7040.00, 7458.62, 7902.13]
        self.nomsNotes = [ ["do"],  ["dod", "reb"], ["re"], ["red", "mib"],
                            ["mi"], ["fa"], ["fad", "solb"], ["sol"], ["sold", "lab"],
               ["la"], ["lad", "sib"], ["si"]]
        self.setBPM(bpm)
    # Definition de chacune des notes du clavier (leur frequence).
    # Paramètre: 
    # * octave: l'octave où le son est joue. L'octave est comprise entre 0 et 7.
    #   Plus cette valeur est petite, plus le son est grave.
    def do(self, octave=2):
        return self.note(octave, 0)
    def dod(self, octave=2):
        return self.note(octave, 1)

    def reb(self, octave=2):
        return self.dod(octave)
    def re(self, octave=2):
        return self.note(octave, 2)
    def red(self, octave=2):
        return self.note(octave, 3)

    def mib(self, octave=2):
        return self.red(octave)
    def mi(self, octave=2):
        return self.note(octave, 4)

    def fa(self, octave=2):
        return self.note(octave, 5)
    def fad(self, octave=2):
        return self.note(octave, 6)

    def solb(self, octave=2):
        return self.fad(octave)
    def sol(self, octave=2):
        return self.note(octave, 7)
    def sold(self, octave=2):
        return self.note(octave, 8)

    def lab(self, octave=2):
        return self.sold(octave)
    def la(self, octave=2):
        return self.note(octave, 9)

    def lad(self, octave=2):
        return self.note(octave, 10)

    def sib(self, octave=2):
        return self.lad(octave)
    def si(self, octave=2):
        return self.note(octave, 11)

    def note(self, octave, idNote):
      return self.frequences[idNote] / (2 ** (7 - octave))
    
    # converti une note en son écriture en LaTeX
    # si minOctave et maxOctave sont différents de None, il s'agit
    # des octaves minimum et maximum qui seront utilisées dans le morceau
    # et qui entraîneront une notation simplifiée s'il y a peu d'amplitude
    def toLaTeX(self, octave, idNote, minOctave = None, maxOctave = None):
      notes = [ ["do"], ["do$\\sharp$"], ["ré"], ["mi$\\flat$"],
               ["mi"], ["fa"], ["fa$\\sharp$"], ["sol"], ["sol$\\sharp$"],
               ["la"], ["si$\\flat$"], ["si"]]
      if (not minOctave is None) and (not maxOctave is None):
        if minOctave == maxOctave:
          return ", ".join([str(n) for n in notes[idNote]])
        elif maxOctave - minOctave == 1:
          if octave == minOctave:
            return ", ".join([str(n) + "$_\\bullet$" for n in notes[idNote]])
          else:
            return ", ".join([str(n) + "$^\\bullet$" for n in notes[idNote]])
      # otherwise, full version
      return ", ".join([n + " $^" + str(octave) + "$" for n in notes[idNote]])

    # definition de la frequence du silence
    def silence(self):
        return 0.

    def getIDNoteDepuisNom(self, nom):
      for i, noms in enumerate(self.nomsNotes):
        if nom in noms:
          return i
      return -1

    # definition des pauses (un silence joue pendant une duree donnee)
    def pause(self):
        return [ self.silence(), self.ronde ]
    def demiePause(self):
        return [ self.silence(), self.blanche ]
    def soupir(self):
        return [ self.silence(), self.noire ]
    def demiSoupir(self):
        return [ self.silence(), self.croche ]
    def quartDeSoupir(self):
        return [ self.silence(), self.doublecroche ]

    # étant donnée une fréquence, retourne l'octave et l'identifiant 
    # de la note correspondante
    def getOctaveEtIDNote(self, note):
      for o in range(-2, 9):
        if note < self.do(o + 1) and note >= self.do(o):
          for i in range(len(self.frequences)):
            if self.note(o, i) == note:
              return (o, i)
      print("erreur, note inconnue: " + str(note))
      return (-1, -1)

    # converti une note donnée en fréquence en son écriture LaTeX
    # si minOctave et maxOctave sont différents de None, il s'agit
    # des octaves minimum et maximum qui seront utilisées dans le morceau
    # et qui entraîneront une notation simplifiée s'il y a peu d'amplitude
    def frequenceToLaTeX(self, frequence, minOctave = None, maxOctave = None):
      (o, i) = self.getOctaveEtIDNote(frequence)
      return self.toLaTeX(o, i, minOctave, maxOctave)
    
    def dureeToMusicTex(self, duree):
      if duree == self.ronde:
        return "\\wh"
      elif duree == self.rondepointee:
        return "\\whp"
      elif duree == self.blanche:
        return "\\ha"
      elif duree == self.blanchepointee:
        return "\\hap"
      elif duree == self.noire:
        return "\\qa"
      elif duree == self.noirepointee:
        return "\\qap"
      elif duree == self.croche:
        return "\\ca"
      elif duree == self.crochepointee:
        return "\\clp"
      elif duree == self.doublecroche:
        return "\\cca"
      elif duree == self.doublecrochepointee:
        return "\\cclp"
      elif duree == self.triplecroche:
        return "\\ccca"
      elif duree == self.triplecrochepointee:
        return "\\ccclp"
    
    def toMusicTeX(self, octave, idNote, octaveRef):
      # voir https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_de_la_musique
      if octave == octaveRef:
        notes = [ "c", "^c", "d", "^d",
                "e", "f", "^f", "g", "^g",
                "h", "^h", "i"]
        return notes[idNote]
      elif octave - octaveRef == 1:
        notes = [ "j", "^j", "k", "^k",
                "l", "m", "^m", "n", "^n",
                "o", "^o", "p"]
        return notes[idNote]
      elif octave - octaveRef == -1:
        notes = [ "J", "^J", "K", "^K",
                "L", "M", "^M", "N", "^N",
                "a", "^a", "b"]
        return notes[idNote]
      elif octave - octaveRef == -2:
        notes = [ "C", "^C", "D", "^D",
                "E", "F", "^F", "G", "^G",
                "H", "^H", "I"]
        return notes[idNote]
      elif octave - octaveRef == 2:
        notes = [ "q", "^q", "r", "^r",
                "s", "t", "^t", "u", "^u",
                "v", "^v", "w"]
        return notes[idNote]
      
    def frequenceToMusicTeX(self, frequence, octaveRef):
      (o, i) = self.getOctaveEtIDNote(frequence)
      return self.toMusicTeX(o, i, octaveRef)

    # retourne l'octave et l'identifiant de la note pour chacune des notes de l'intervalle donné en fréquence
    def getOctaveEtIDNoteDansIntervalle(self, noteMin, noteMax):
      if noteMin > noteMax:
        return self.getOctaveEtIDNoteDansIntervalle(noteMax, noteMin)
      (octaveMin, idNoteMin) = self.getOctaveEtIDNote(noteMin)
      (octaveMax, idNoteMax) = self.getOctaveEtIDNote(noteMax)

      if octaveMin == octaveMax:
        return [(octaveMin, freq) for freq in range(idNoteMin, idNoteMax + 1)]

      result = [(octaveMin, freq) for freq in range(idNoteMin, len(self.frequences))]
      for o in range(octaveMin + 1, octaveMax):
        result += [(o, freq) for freq in range(0, len(self.frequences))]
      result += [(octaveMax, freq) for freq in range(0, idNoteMax + 1)]
      return result

    # retourne la liste des notes dans l'intervalle donné
    def notesDansIntervalle(self, noteMin, noteMax, uneSeuleParOctave = False):
      if uneSeuleParOctave:
        return [ self.note(o, i) for (o, i) in self.getOctaveEtIDNoteDansIntervalle(noteMin, noteMax) if i == 0 ]
      else:
        return [ self.note(o, i) for (o, i) in self.getOctaveEtIDNoteDansIntervalle(noteMin, noteMax) ]
      
    def notesDansPartition(self, partitions):
      l = [[ [x] if isinstance(x, float) else x for x in [note[0] if not isinstance(note[0], float) else note[0] for note in partition]] for partition in partitions]
      return [x for x in sum(sum(l, []), []) if x != 0]
    
    def contientSilences(self, partitions):
      l = [[ [x] if isinstance(x, float) else x for x in [note[0] if not isinstance(note[0], float) else note[0] for note in partition]] for partition in partitions]
      return len([x for x in sum(sum(l, []), []) if x == 0]) != 0
      

    # retourne la fréquence minimum utilisée dans les partitions
    def minFrequence(self, partitions):
      return min(self.notesDansPartition(partitions))

    # retourne la fréquence maximum utilisée dans les partitions
    def maxFrequence(self, partitions):
      return max(self.notesDansPartition(partitions))


    def durees(self, partitions):
      d = set()
      for partition in partitions:
        for n in partition:
          d.add(n[1])
      return sorted(list(d))

    # Etant donne un nombre de battements par minute, cette fonction fixe la duree
    # de chacune des notes, exprimée en secondes
    # Paramètre:
    # * bpm: nombre de battements par minute (= nombre de noires par minute)
    def setBPM(self, bpm):
        self.bpm = bpm
        self.noire = 60. / self.bpm
        self.croche = self.noire / 2 
        self.doublecroche = self.croche / 2
        self.triplecroche = self.croche / 3

        self.blanche = 2 * self.noire
        self.ronde = 2 * self.blanche
        self.carree = 2 * self.ronde

        self.noirepointee = self.noire * 1.5
        self.crochepointee = self.croche * 1.5
        self.doublecrochepointee = self.doublecroche * 1.5

        self.blanchepointee = self.blanche * 1.5
        self.rondepointee = self.ronde * 1.5
        self.carreepointee = self.carree * 1.5
    
    def nomDuree(self, duree):
      noms = { self.noire: "noire", self.croche: "croche", self.doublecroche: "double croche",
              self.triplecroche: "triple croche", self.blanche: "blanche", self.ronde: "ronde",
              self.carree: "carrée", self.noirepointee: "noire pointée", self.crochepointee: "croche pointée",
              self.doublecrochepointee: "double croche pointée", self.blanchepointee: "blanche pointée",
              self.rondepointee: "ronde pointée", self.carreepointee: "carrée pointée" }
      if duree in noms:
        return noms[duree]
      else:
        return "inconnue"
      
    def dureeEnNoires(self, duree):
      return duree / self.noire
      
    # d'après wikipédia, «le triolet est une division exceptionnelle du temps, formée d'un 
    #groupe de trois figures égales dont la somme équivaut à deux figures identiques dans un 
    # temps normalement binaire. »
    def triolet(self, duree):
      return duree * 2 / 3
