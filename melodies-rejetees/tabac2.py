#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème j\'ai du bon tabac.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(117)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


tabac = [ [ [gamme.do(3), gamme.croche],
            [gamme.re(3), gamme.croche],
            [gamme.mi(3), gamme.croche],
            [gamme.do(3), gamme.croche],
            
            [gamme.re(3), gamme.noire],
            [gamme.re(3), gamme.croche],
            [gamme.mi(3), gamme.croche],
             
            [gamme.fa(3), gamme.noire],
            [gamme.fa(3), gamme.noire],
            
            [gamme.mi(3), gamme.noire],
            [gamme.mi(3), gamme.noire],
            
            [gamme.do(3), gamme.croche],
            [gamme.re(3), gamme.croche],
            [gamme.mi(3), gamme.croche],
            [gamme.do(3), gamme.croche],
            
            [gamme.re(3), gamme.noire],
            [gamme.re(3), gamme.croche],
            [gamme.mi(3), gamme.croche],
            
            [gamme.fa(3), gamme.noire],
            [gamme.sol(3), gamme.noire],
            
            [gamme.do(3), gamme.blanche]
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(8)
  g.setLongueurPartitionMusicale(8)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(tabac, args.exerciceSpectrogramme[0], "thème 3")
  if args.transparent:
    g.genererTransparentSpectrogramme(tabac, args.transparent[0], "thème 3", "j'ai du bon tabac")
  if args.correction:
    g.genererCorrectionSpectrogramme(tabac, args.correction[0], "thème 3", "j'ai du bon tabac")
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(tabac, "sons/tabac2.wav")

