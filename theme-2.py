#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème au clair de la lune.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(80)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


lune = [ [ [gamme.do(4), gamme.croche],
           [gamme.do(4), gamme.croche],
           [gamme.do(4), gamme.croche],
           [gamme.re(4), gamme.croche],
           
           [gamme.mi(4), gamme.noire],
           [gamme.re(4), gamme.noire],
           
           [gamme.do(4), gamme.croche],
           [gamme.mi(4), gamme.croche],
           [gamme.re(4), gamme.croche],
           [gamme.re(4), gamme.croche],
           
           [gamme.do(4), gamme.blanche],
           
           
           
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(4)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(lune, args.exerciceSpectrogramme[0], "thème 2")
  if args.transparent:
    g.genererTransparentSpectrogramme(lune, args.transparent[0], "thème 2", "Au clair de la lune")
  if args.correction:
    g.genererCorrectionSpectrogramme(lune, args.correction[0], "thème 2", "Au clair de la lune")

else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(lune, "sons/lune.wav")

