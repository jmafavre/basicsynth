# Basic synth

Écrit en python, *basic synth* est un synthétiseur de son, qui simule naïvement 
un piano. La génération fonctionne par synthèse additive, en transformant une 
série de notes en un ensemble d'oscillations.

Chaque note est transformée en une oscillation sinusoïdale, et la progression de 
son volume contrôlée par une 
[enveloppe sonore](https://fr.wikipedia.org/wiki/Enveloppe_sonore). Chaque note
est également accompagnée 
d'[harmoniques](https://fr.wikipedia.org/wiki/Harmonique_(musique)) venant
enrichir le son un peu pauvre d'une simple sinusoïde.

L'outil permet de jouer des accords, et prend en charge plusieurs partitions 
simultanées, pour un jeu à plusieurs mains.

Quelques scripts python sont donnés à titre d'exemple pour découvrir 
comment utiliser simplement ce synthétiseur.

## En savoir plus

Pour en savoir plus sur la synthèse additive, et sur ce synthétiseur, on peut par 
exemple lire [un billet sur le blog de Jean-Marie Favreau](http://jmtrivial.info/blog/2017/12/23/la-synthese-de-son-additive/).
