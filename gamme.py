#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de la gamme.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(117)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


gammelist = [ [ 
      [gamme.do(4), gamme.noire],
      [gamme.dod(4), gamme.noire],
      [gamme.re(4), gamme.noire],
      [gamme.mib(4), gamme.noire],
      [gamme.mi(4), gamme.noire],
      [gamme.fa(4), gamme.noire],
      [gamme.fad(4), gamme.noire],
      [gamme.sol(4), gamme.noire],
      [gamme.sold(4), gamme.noire],
      [gamme.la(4), gamme.noire],
      [gamme.sib(4), gamme.noire],
      [gamme.si(4), gamme.noire],
      [gamme.do(5), gamme.noire],
      [gamme.dod(5), gamme.noire],
      [gamme.re(5), gamme.noire],
      [gamme.mib(5), gamme.noire],
      [gamme.mi(5), gamme.noire],
      [gamme.fa(5), gamme.noire],
      [gamme.fad(5), gamme.noire],
      [gamme.sol(5), gamme.noire],
      [gamme.sold(5), gamme.noire],
      [gamme.la(5), gamme.noire],
      [gamme.sib(5), gamme.noire],
      [gamme.si(5), gamme.noire],
      ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(5)
  g.setLongueurPartitionMusicale(5)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(gammelist, args.exerciceSpectrogramme[0], "thème 0")
  if args.transparent:
    g.genererTransparentSpectrogramme(gammelist, args.transparent[0], "thème 0", "Gamme")
  if args.correction:
    g.genererCorrectionSpectrogramme(gammelist, args.correction[0], "thème 0", "Gamme")
  
else:
  print("Génération du son")
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(gammelist, "sons/gamme.wav")

