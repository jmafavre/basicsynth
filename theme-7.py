#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème Le flic de Beverly Hills.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(117)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


flic = [ [ 
      [gamme.fad(4), gamme.noire],
    [gamme.la(4), gamme.crochepointee],
    [gamme.fad(4), gamme.doublecroche],
    [gamme.fad(4), gamme.doublecroche],
    [gamme.si(4), gamme.doublecroche],
    [gamme.fad(4), gamme.croche],
    [gamme.mi(4), gamme.croche],
    [gamme.fad(4), gamme.crochepointee],
    [gamme.re(5), gamme.croche],
    [gamme.fad(4), gamme.doublecroche],
    [gamme.fad(4), gamme.croche],
    [gamme.re(4), gamme.doublecroche],
    [gamme.dod(5), gamme.doublecroche],
    [gamme.la(4), gamme.doublecroche],
    [gamme.fad(4), gamme.doublecroche],
    [gamme.dod(5), gamme.croche],
    [gamme.fad(5), gamme.doublecroche],
    [gamme.fad(4), gamme.doublecroche],
    [gamme.mi(4), gamme.croche],
    [gamme.mi(4), gamme.croche],
    [gamme.dod(4), gamme.doublecroche],
    [gamme.sold(4), gamme.doublecroche],
    [gamme.fad(4), gamme.blanche]
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(5)
  g.setLongueurPartitionMusicale(5)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(flic, args.exerciceSpectrogramme[0], "thème 7")
  if args.transparent:
    g.genererTransparentSpectrogramme(flic, args.transparent[0], "thème 7", "Le flic de Beverly Hills")
  if args.correction:
    g.genererCorrectionSpectrogramme(flic, args.correction[0], "thème 7", "Le flic de Beverly Hills")
  
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(flic, "sons/flic.wav")

