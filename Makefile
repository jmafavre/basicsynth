
EXERCICESDIR=exercices/
SONS=sons/
SCRIPTS=$(wildcard *.py)
EXECOLLTEX=$(addprefix $(EXERCICESDIR)/ex-college-,$(SCRIPTS:.py=.tex))
EXECOLLPDF=$(EXECOLLTEX:.tex=.pdf)
EXEPRIMTEX=$(addprefix $(EXERCICESDIR)/ex-primaire-,$(SCRIPTS:.py=.tex))
EXEPRIMPDF=$(EXEPRIMTEX:.tex=.pdf)
EXEPROJTEX=$(addprefix $(EXERCICESDIR)/ex-proj-,$(SCRIPTS:.py=.tex))
EXEPROJPDF=$(EXEPROJTEX:.tex=.pdf)
EXECORRTEX=$(addprefix $(EXERCICESDIR)/ex-correction-,$(SCRIPTS:.py=.tex))
EXECORRPDF=$(EXECORRTEX:.tex=.pdf)

TEX=$(EXECOLLTEX) $(EXEPROJTEX) $(EXEPRIMTEX) $(EXECORRTEX)
PDF=$(EXECOLLPDF) $(EXEPROJPDF) $(EXEPRIMPDF) $(EXECORRPDF)

WAVES=$(addprefix $(SONS)/,$(SCRIPTS:.py=.wav))

BOOMWHACKERS=boomwhackers-8+5+c.json

.PRECIOUS: $(TEX)

all: waves pdf

waves: $(WAVES)

pdf: $(PDF)

tex: $(TEX)

$(EXERCICESDIR)/%.pdf: $(EXERCICESDIR)/%.tex
	cd $(EXERCICESDIR); pdflatex $(<:$(EXERCICESDIR)/%=%); cd ..
	
$(EXERCICESDIR)/ex-college-%.tex: %.py
	python $< --exercice-spectrogramme $@ --boomwhackers=$(BOOMWHACKERS)

$(EXERCICESDIR)/ex-primaire-%.tex: %.py
	python $< --exercice-spectrogramme $@ --primaire --boomwhackers=$(BOOMWHACKERS)

	
$(EXERCICESDIR)/ex-proj-%.tex: %.py
	python $< --exercice-spectrogramme-transparent $@ --boomwhackers=$(BOOMWHACKERS)

$(EXERCICESDIR)/ex-correction-%.tex: %.py
	python $< --exercice-spectrogramme-correction $@ --boomwhackers=$(BOOMWHACKERS)


$(SONS)/%.wav: %.py
	python $< 
	
cleantex:
	rm -f $(TEX)
	

cleanpdf:
	rm -f $(PDF)

cleanwaves:
	rm -f $(WAVES)
	
clean: cleantex cleanpdf cleanwaves
	
