#!/usr/bin/env python
# coding: utf8

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de "la lettre à Élise".')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)
args = parser.parse_args()

gamme = GammeTemperee(95)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


elise =  [
          [ [gamme.mi(3), gamme.doublecroche], 
          [gamme.red(3), gamme.doublecroche],
          [gamme.mi(3), gamme.doublecroche],
          [gamme.red(3), gamme.doublecroche],
          [gamme.mi(3), gamme.doublecroche],
          [gamme.si(2), gamme.doublecroche],
          [gamme.re(3), gamme.doublecroche],
          [gamme.do(3), gamme.doublecroche],
          [gamme.la(2), gamme.croche],
          [gamme.silence(), gamme.croche],
          [gamme.do(2), gamme.doublecroche],
          [gamme.mi(2), gamme.doublecroche],
          [gamme.la(2), gamme.doublecroche],
          [gamme.si(2), gamme.croche],
          [gamme.silence(), gamme.doublecroche],
          [gamme.mi(2), gamme.doublecroche],
          [gamme.sold(2), gamme.doublecroche],
          [gamme.si(2), gamme.doublecroche],
          [gamme.do(3), gamme.croche],
          [gamme.silence(), gamme.doublecroche],
          [gamme.mi(2), gamme.doublecroche],
          [gamme.mi(3), gamme.doublecroche],
          [gamme.red(3), gamme.doublecroche],
          [gamme.mi(3), gamme.doublecroche],
          [gamme.red(3), gamme.doublecroche],
          [gamme.mi(3), gamme.doublecroche],
          [gamme.si(2), gamme.doublecroche],
          [gamme.re(3), gamme.doublecroche],
          [gamme.do(3), gamme.doublecroche],
          [gamme.la(2), gamme.croche],
          [gamme.silence(), gamme.doublecroche],          
          [gamme.do(2), gamme.doublecroche],
          [gamme.mi(2), gamme.doublecroche],
          [gamme.la(2), gamme.doublecroche],
          [gamme.si(2), gamme.croche],
          [gamme.silence(), gamme.doublecroche],          
          [gamme.mi(2), gamme.doublecroche],
          [gamme.do(3), gamme.doublecroche],
          [gamme.si(2), gamme.doublecroche],
          [gamme.la(2), gamme.croche]
          ]]

if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(3)
  g.setLongueurPartitionMusicale(5)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(elise, args.exerciceSpectrogramme[0], "thème 6")
  if args.transparent:
    g.genererTransparentSpectrogramme(elise, args.transparent[0], "thème 6", "La lettre à Élise")
  if args.correction:
    g.genererCorrectionSpectrogramme(elise, args.correction[0], "thème 6", "La lettre à Élise")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 3)
  s.genererMelodie(elise, "sons/elise.wav")
