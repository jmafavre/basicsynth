#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème du flic de Beverly Hills (v2).')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(80)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

flic2 = [ [ [gamme.mi(4), gamme.croche],
[gamme.sol(4), gamme.doublecrochepointee],
[gamme.mi(4), gamme.doublecroche],
[gamme.mi(4), gamme.triplecroche],
[gamme.la(4), gamme.doublecroche],
[gamme.mi(4), gamme.doublecroche],
[gamme.red(4), gamme.doublecroche],
[gamme.mi(4), gamme.croche],
[gamme.si(4), gamme.doublecrochepointee],
[gamme.mi(4), gamme.doublecroche],
[gamme.mi(4), gamme.triplecroche],
[gamme.do(3), gamme.doublecroche],
[gamme.si(4), gamme.doublecroche],
[gamme.sol(4), gamme.doublecroche],
[gamme.mi(4), gamme.doublecroche],
[gamme.si(4), gamme.doublecroche],
[gamme.mi(3), gamme.doublecroche],
[gamme.mi(4), gamme.triplecroche],
[gamme.red(4), gamme.doublecroche],
[gamme.red(4), gamme.triplecroche],
[gamme.si(1), gamme.doublecroche],
[gamme.fad(4), gamme.doublecroche],
[gamme.mi(4), gamme.doublecroche]
                     ] ]

if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(flic2, "exercices/ex-spec-flic2.tex", "thème \"le flic de Beverly Hills\"")
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(flic2, "sons/flic2.wav")

