#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

from musique.basicsynth import BasicSequenceur


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de Frère Jacques.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(160)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

frerejacques = [ [ [gamme.sol(4), gamme.noire],
                   [gamme.la(4), gamme.noire],
                   [gamme.si(4), gamme.noire], 
                   [gamme.sol(4), gamme.noire],
                   
                   [gamme.sol(4), gamme.noire],
                   [gamme.la(4), gamme.noire],
                   [gamme.si(4), gamme.noire], 
                   [gamme.sol(4), gamme.noire], 
                   
                   [gamme.si(4), gamme.noire],
                   [gamme.do(5), gamme.noire],
                   [gamme.re(5), gamme.blanche], 
                
                   [gamme.si(4), gamme.noire],
                   [gamme.do(5), gamme.noire],
                   [gamme.re(5), gamme.blanche], 

                   [gamme.re(5), gamme.croche],
                   [gamme.mi(5), gamme.croche],
                   [gamme.re(5), gamme.croche],
                   [gamme.do(5), gamme.croche],
                   [gamme.si(4), gamme.noire],
                   [gamme.sol(4), gamme.noire],

                   [gamme.re(5), gamme.croche],
                   [gamme.mi(5), gamme.croche],
                   [gamme.re(5), gamme.croche],
                   [gamme.do(5), gamme.croche],
                   [gamme.si(4), gamme.noire],
                   [gamme.sol(4), gamme.noire],

                   [gamme.sol(4), gamme.noire],
                   [gamme.re(4), gamme.noire],
                   [gamme.sol(4), gamme.blanche], 

                   [gamme.sol(4), gamme.noire],
                   [gamme.re(4), gamme.noire],
                   [gamme.sol(4), gamme.blanche]
                ] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(frerejacques, "exercices/ex-spec-frerejacques.tex", "thème \"Frère Jacques\"")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 2)
  s.genererMelodie(frerejacques, "sons/frerejacques.wav")

