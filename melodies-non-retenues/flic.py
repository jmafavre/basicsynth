#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

from musique.basicsynth import BasicSequenceur


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème du flic de Beverly Hills.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(117)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

flic = [ [ [gamme.fad(4), gamme.noire], 
          [gamme.la(4), gamme.crochepointee],
          [gamme.fad(4), gamme.doublecroche],
          [gamme.fad(4), gamme.doublecroche],
          [gamme.si(4), gamme.doublecroche],
          [gamme.fad(4), gamme.croche],
          [gamme.mi(4), gamme.croche],
          [gamme.fad(4), gamme.crochepointee],
          [gamme.re(5), gamme.croche],
          [gamme.fad(4), gamme.doublecroche],
          [gamme.fad(4), gamme.croche],
          [gamme.re(4), gamme.doublecroche],
          [gamme.dod(5), gamme.doublecroche],#reb
          [gamme.la(4), gamme.doublecroche],
          [gamme.fad(4), gamme.doublecroche],
          [gamme.dod(5), gamme.croche],
          [gamme.fad(5), gamme.doublecroche],
          [gamme.fad(4), gamme.doublecroche],
          [gamme.mi(4), gamme.croche],
          [gamme.mi(4), gamme.croche],
          [gamme.dod(4), gamme.doublecroche],
          [gamme.sold(4), gamme.doublecroche],#lab
          [gamme.fad(4), gamme.blanche]          ] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(flic, "exercices/ex-spec-flic.tex", "thème \"le flic de Beverly Hills\"")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 5)
  s.genererMelodie(flic, "sons/flic.wav")

