#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de Stairway.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(117)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

stair = [ [
    [gamme.la(2), gamme.croche],
[gamme.do(3), gamme.croche],
[gamme.mi(3), gamme.croche],
[gamme.la(3), gamme.croche],
[[gamme.fad(2), gamme.si(3)], gamme.croche],
[gamme.mi(3), gamme.croche],
[gamme.do(3), gamme.croche],
[gamme.si(3), gamme.croche],
[[gamme.fa(2), gamme.do(4)], gamme.croche],
[gamme.mi(3), gamme.croche],
[gamme.do(3), gamme.croche],
[gamme.do(4), gamme.croche],
[[gamme.fad(2), gamme.fad(3)], gamme.croche],
[gamme.re(3), gamme.croche],
[gamme.la(2), gamme.croche],
[gamme.fa(3), gamme.croche],
[[gamme.fa(2), gamme.mi(3)], gamme.croche],
[gamme.do(3), gamme.croche],
[gamme.la(2), gamme.croche],
[gamme.do(3), gamme.noire],
[gamme.mi(3), gamme.croche],
[gamme.do(3), gamme.croche],
[gamme.la(2), gamme.croche],
[[gamme.si(1), gamme.sol(2), gamme.si(2)], gamme.croche],
[[gamme.la(2), gamme.do(3)], gamme.croche],
[[gamme.la(1), gamme.la(2)], gamme.blanche]
        ] ]

if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(stair, "exercices/ex-spec-stairway.tex", "Stairway")
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(stair, "sons/stairway.wav")

