#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème joyeux anniversaire.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

gamme = GammeTemperee(117)


happy = [ [ [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.re(4), gamme.noire],
            [gamme.do(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.mi(4), gamme.blanche],
            [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.re(4), gamme.noire],
            [gamme.do(4), gamme.noire],
            [gamme.sol(4), gamme.noire],
            [gamme.fa(4), gamme.blanche],
            [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.do(5), gamme.noire],
            [gamme.la(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.mi(4), gamme.noire],
            [gamme.re(4), gamme.noire],
            [gamme.sib(4), gamme.crochepointee],
            [gamme.sib(4), gamme.doublecroche],
            [gamme.la(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.sol(4), gamme.noire],
            [gamme.fa(4), gamme.blanchepointee]
        ] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(happy, "exercices/ex-spec-happybirthday.tex", "thème joyeux anniversaire")
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 8)
  s.genererMelodie(happy, "sons/happybirthday.wav")

