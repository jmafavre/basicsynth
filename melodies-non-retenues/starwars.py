#!/usr/bin/env python
# coding: utf8

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème de star wars.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)
args = parser.parse_args()

gamme = GammeTemperee(150)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


starwars =  [
    [ [gamme.re(2), gamme.croche],
    [gamme.sol(2), gamme.blanche],
    [gamme.la(2), gamme.noire],
    [gamme.sib(2), gamme.croche],
    [gamme.do(3), gamme.croche],
    [gamme.sib(2), gamme.blanche],
    [gamme.re(2), gamme.noirepointee],
    [gamme.re(2), gamme.croche],
    [gamme.sol(2), gamme.noirepointee],
    [gamme.la(2), gamme.croche],
    [gamme.sib(2), gamme.croche],
    [gamme.sol(2), gamme.croche],
    [gamme.sib(2), gamme.croche],
    [gamme.sol(2), gamme.croche],
    [gamme.re(3), gamme.croche],
    [gamme.do(3), gamme.blanche],
    [gamme.re(2), gamme.croche],
    [gamme.re(2), gamme.croche],
    [gamme.re(2), gamme.croche],
    [gamme.sol(2), gamme.noirepointee],
    [gamme.la(2), gamme.croche],
    [gamme.sib(2), gamme.croche],
    [gamme.sol(2), gamme.croche],
    [gamme.sib(2), gamme.croche],
    [gamme.sol(2), gamme.croche],
    [gamme.sol(3), gamme.blanche],
    [gamme.sol(2), gamme.noire],
    [gamme.sib(2), gamme.croche],
    [gamme.la(2), gamme.doublecroche],
    [gamme.sol(2), gamme.doublecroche],
    [gamme.re(3), gamme.noire],
    [gamme.sib(2), gamme.croche],
    [gamme.sol(2), gamme.croche],
    [gamme.re(2), gamme.noirepointee],
    [gamme.re(2), gamme.croche],
    [gamme.sol(2), gamme.blanche]
          ]]

if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(starwars, "exercices/ex-spec-starwars.tex", "thème StarWars")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 3)
  s.genererMelodie(starwars, "sons/starwars.wav")
