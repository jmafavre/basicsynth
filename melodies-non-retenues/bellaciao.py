#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

from musique.basicsynth import BasicSequenceur


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de Bella Ciao.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(100)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]
  
bellaciao = [ [ [gamme.mi(4), gamme.croche],
                [gamme.la(4), gamme.croche],
                [gamme.si(4), gamme.croche],
                [gamme.do(5), gamme.croche],
                [gamme.la(4), gamme.blanche],
                
                [gamme.mi(4), gamme.croche],
                [gamme.la(4), gamme.croche],
                [gamme.si(4), gamme.croche],
                [gamme.do(5), gamme.croche],
                [gamme.la(4), gamme.blanche],
                
                [gamme.mi(4), gamme.croche],
                [gamme.la(4), gamme.croche],
                [gamme.si(4), gamme.croche],
                [gamme.do(5), gamme.noire],
                
                [gamme.si(4), gamme.croche],
                [gamme.la(4), gamme.croche],
                [gamme.do(5), gamme.noire],
                
                [gamme.si(4), gamme.croche],
                [gamme.la(4), gamme.croche],
                
                [gamme.mi(5), gamme.noire],
                [gamme.mi(5), gamme.noire],
                [gamme.mi(5), gamme.croche],
                
                
                [gamme.re(5), gamme.croche],
                [gamme.re(5), gamme.croche],
                [gamme.mi(5), gamme.croche],
                [gamme.fa(5), gamme.croche],
                [gamme.fa(5), gamme.croche],
                [gamme.fa(5), gamme.noirepointee],
                
                [gamme.fa(5), gamme.croche],
                [gamme.mi(5), gamme.croche],
                [gamme.re(5), gamme.croche],
                [gamme.mi(5), gamme.croche],
                [gamme.mi(5), gamme.croche],
                [gamme.mi(5), gamme.noirepointee],

                [gamme.re(5), gamme.croche],
                [gamme.re(5), gamme.croche],
                [gamme.do(5), gamme.croche],
                [gamme.si(4), gamme.noire],
                [gamme.mi(5), gamme.noire],
                [gamme.do(5), gamme.noire],
                [gamme.si(4), gamme.noire],
                [gamme.la(4), gamme.blanche],
                ] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(bellaciao, "exercices/ex-spec-bellaciao.tex", "thème \"Bella Ciao\"")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 2)
  s.genererMelodie(bellaciao, "sons/bellaciao.wav")

