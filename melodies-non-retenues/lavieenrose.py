#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de "la vie en rose".')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(160)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

lavieenrose = [ [ [gamme.do(2), gamme.noirepointee],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.croche],
                  [gamme.sol(1), gamme.croche],
                  [gamme.mi(1), gamme.croche],
                  [gamme.do(2), gamme.croche],
                  [gamme.si(1), gamme.noirepointee],
                  [gamme.la(1), gamme.croche],
                  [gamme.sol(1), gamme.croche],
                  [gamme.mi(1), gamme.croche],
                  [gamme.do(2), gamme.croche],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.noirepointee],
                  [gamme.sol(1), gamme.croche],
                  [gamme.mi(1), gamme.croche],
                  [gamme.do(1), gamme.croche],
                  [gamme.do(1), gamme.croche],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.blanche],
                  [gamme.sol(1), gamme.blanche],
                  [gamme.re(2), gamme.noirepointee],
                  [gamme.do(2), gamme.croche],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.croche],
                  [gamme.fa(1), gamme.croche],
                  [gamme.do(2), gamme.croche],
                  [gamme.si(1), gamme.noirepointee],
                  [gamme.la(1), gamme.croche],
                  [gamme.sol(1), gamme.croche],
                  [gamme.mi(1), gamme.croche],
                  [gamme.do(2), gamme.croche],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.noirepointee],
                  [gamme.sol(1), gamme.croche],
                  [gamme.mi(1), gamme.croche],
                  [gamme.do(1), gamme.croche],
                  [gamme.do(1), gamme.croche],
                  [gamme.si(1), gamme.croche],
                  [gamme.la(1), gamme.blanche],
                  [gamme.sol(1), gamme.noirepointee],
                  [[gamme.sol(1), gamme.si(1)], gamme.croche],
                  [[gamme.do(1), gamme.do(2)], gamme.noire] ] ]



if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(lavieenrose, "exercices/ex-spec-lavieenrose.tex", "La vie en rose")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm , 5)
  s.genererMelodie(lavieenrose, "sons/lavieenrose.wav")
