#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de "rainbow".')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(130)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

rainbow = [ [ [gamme.do(3), gamme.blanche],
[gamme.do(3), gamme.noire],
[gamme.si(2), gamme.noire],
[gamme.sol(2), gamme.croche],
[gamme.la(2), gamme.croche],
[gamme.si(2), gamme.noire],
[gamme.do(3), gamme.blanche],
[gamme.do(2), gamme.noirepointee],
[gamme.la(2), gamme.croche],
[gamme.sol(2), gamme.blanche],
gamme.pause(),
[gamme.la(1), gamme.blanche],
[gamme.fa(2), gamme.blanche],
[gamme.mi(2), gamme.noire],
[gamme.do(2), gamme.croche],
[gamme.re(2), gamme.croche],
[gamme.mi(2), gamme.noire],
[gamme.fa(2), gamme.noire],
[gamme.re(2), gamme.noire],
[gamme.sol(2), gamme.croche],
[gamme.si(2), gamme.croche],
[gamme.do(2), gamme.croche],
[gamme.re(2), gamme.noirepointee],
[gamme.do(2), gamme.croche]
        ] ]

if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(rainbow, "exercices/ex-spec-rainbow.tex", "Rainbow")
else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 5)
  s.genererMelodie(rainbow, "sons/rainbow.wav")

