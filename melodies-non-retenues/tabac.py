#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee


parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition "j\'ai du bon tabac".')
parser.add_argument('--full', help="Génère l'ensemble des sons pour illustrer la construction du synthétiseur", dest='full', action='store_true',default=False)
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]


gamme = GammeTemperee(160)

tabac = [ [ [gamme.sol(3), gamme.noire],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.sol(3), gamme.noire],
           [gamme.la(3), gamme.blanche],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.do(4), gamme.blanche],
           [gamme.do(4), gamme.blanche],
           [gamme.si(3), gamme.blanche],
           [gamme.si(3), gamme.blanche],
           [gamme.sol(3), gamme.noire],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.sol(3), gamme.noire],
           [gamme.la(3), gamme.blanche],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.do(4), gamme.blanche],
           [gamme.re(4), gamme.blanche],
           [gamme.sol(3), gamme.ronde],
           [gamme.re(4), gamme.blanche],
           [gamme.re(4), gamme.noire],
           [gamme.do(4), gamme.noire],
           [gamme.si(3), gamme.blanche],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.do(4), gamme.blanche],
           [gamme.re(4), gamme.blanche],
           [gamme.la(3), gamme.ronde],
           [gamme.re(4), gamme.blanche],
           [gamme.re(4), gamme.noire],
           [gamme.do(4), gamme.noire],
           [gamme.si(3), gamme.blanche],
           [gamme.la(3), gamme.noire],
           [gamme.si(3), gamme.noire],
           [gamme.do(4), gamme.blanche],
           [gamme.re(4), gamme.blanche],
           [gamme.la(3), gamme.ronde]
           ] ]


s = BasicSequenceur(1, gamme.bpm , 0)


if args.full:
  print "Génération de multiples versions"
  s.setEnveloppeZeroUn(0.3)
  s.genererMelodie(tabac, "sons/tabac-01.wav")

  s.setEnveloppeMonteeDescente(0.5)
  s.genererMelodie(tabac, "sons/tabac-md.wav")

  s.setEnveloppeFrappeSimple(0.7)
  s.nbHarmoniques = 0
  s.genererMelodie(tabac, "sons/tabac-frappe.wav")

if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(tabac, "exercices/ex-spec-tabac.tex", "j'ai du bon tabac")
else:
  print "Génération de la version \"piano\""
  s.setEnveloppeFrappeSimple(0.7)
  s.nbHarmoniques = 5
  s.genererMelodie(tabac, "sons/tabac.wav")


