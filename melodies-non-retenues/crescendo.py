#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

from musique.basicsynth import BasicSequenceur


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à \
                                 partir d\'un crescendo.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice \
                    plutôt que le fichier son", dest='exerciceSpectrogramme', 
                    action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(117)

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

crescendo = [ [ [gamme.do(3), gamme.noire],
                [gamme.re(3), gamme.noire], 
                [gamme.mi(3), gamme.noire], 
                [gamme.fa(3), gamme.noire], 
                [gamme.sol(3), gamme.noire], 
                [gamme.la(3), gamme.noire], 
                [gamme.si(3), gamme.noire], 
                [gamme.do(4), gamme.noire]] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(4)
  g.genererExerciceSpectrogramme(crescendo,
                                 "exercices/ex-spec-crescendo.tex",
                                 "Crescendo")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(crescendo, "sons/crescendo.wav")

