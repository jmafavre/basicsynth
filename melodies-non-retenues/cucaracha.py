#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

from musique.basicsynth import BasicSequenceur


import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition de la cucaracha.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', action='store_true',default=False)
parser.add_argument('--boomwhackers', help="Génère un exercice plutôt que le fichier son", dest='boomwhackers', nargs=1)

args = parser.parse_args()

gamme = GammeTemperee(160)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

cucaracha = [ [ [gamme.re(4), gamme.noire], 
                [gamme.re(4), gamme.noire],
                [gamme.re(4), gamme.noire],
                [gamme.sol(4), gamme.noire],
                [gamme.si(4), gamme.noire],
                
                [gamme.re(4), gamme.noire], 
                [gamme.re(4), gamme.noire],
                [gamme.re(4), gamme.noire],
                [gamme.sol(4), gamme.noire],
                [gamme.si(4), gamme.blanche],
                
                [gamme.sol(4), gamme.noire], 
                [gamme.sol(4), gamme.noire],
                [gamme.fad(4), gamme.noire],
                [gamme.fad(4), gamme.noire],
                [gamme.mi(4), gamme.noire],
                [gamme.mi(4), gamme.noire],
                [gamme.re(4), gamme.noirepointee],

                [gamme.re(4), gamme.noire], 
                [gamme.re(4), gamme.noire],
                [gamme.re(4), gamme.noire],
                [gamme.fad(4), gamme.noire],
                [gamme.la(4), gamme.blanche],

                [gamme.re(5), gamme.noire], 
                [gamme.mi(5), gamme.noire],
                [gamme.re(5), gamme.noire],
                [gamme.do(5), gamme.noire],
                [gamme.si(4), gamme.noire],
                [gamme.la(4), gamme.noire],
                [gamme.sol(4), gamme.noirepointee]

                ] ]


if args.exerciceSpectrogramme:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.genererExerciceSpectrogramme(cucaracha, "exercices/ex-spec-cucaracha.tex", "thème \"la cucaracha\"")
else:
  print "Génération de la version \"piano\""
  s = BasicSequenceur(1, gamme.bpm, 2)
  s.genererMelodie(cucaracha, "sons/cucaracha.wav")

