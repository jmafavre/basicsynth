#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème joyeux anniversaire.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

gamme = GammeTemperee(117)


happy = [ [ [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.re(4), gamme.noire],
            [gamme.do(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.mi(4), gamme.blanche],
            [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.re(4), gamme.noire],
            [gamme.do(4), gamme.noire],
            [gamme.sol(4), gamme.noire],
            [gamme.fa(4), gamme.blanche],
            [gamme.do(4), gamme.crochepointee],
            [gamme.do(4), gamme.doublecroche],
            [gamme.do(5), gamme.noire],
            [gamme.la(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.mi(4), gamme.noire],
            [gamme.re(4), gamme.noire],
            [gamme.sib(4), gamme.crochepointee],
            [gamme.sib(4), gamme.doublecroche],
            [gamme.la(4), gamme.noire],
            [gamme.fa(4), gamme.noire],
            [gamme.sol(4), gamme.noire],
            [gamme.fa(4), gamme.blanchepointee]
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(6)
  g.setLongueurPartitionMusicale(12)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(happy, args.exerciceSpectrogramme[0], "thème 5")
  if args.transparent:
    g.genererTransparentSpectrogramme(happy, args.transparent[0], "thème 5", "joyeux anniversaire")
  if args.correction:
    g.genererCorrectionSpectrogramme(happy, args.correction[0], "thème 5", "joyeux anniversaire")

else:
  print "Génération du son"
  s = BasicSequenceur(1, gamme.bpm, 8)
  s.genererMelodie(happy, "sons/happybirthday.wav")

