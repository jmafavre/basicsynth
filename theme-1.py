#!/usr/bin/env python
# coding: utf8
# -*- coding: utf-8 -*-

import argparse
from musique.basicsynth import BasicSequenceur
from musique.exercice import GenerateurExercice
from musique.gamme import GammeTemperee

parser = argparse.ArgumentParser(description='Génère des documents à partir de la partition du thème ode à la joie.')
parser.add_argument('--exercice-spectrogramme', help="Génère un exercice plutôt que le fichier son", dest='exerciceSpectrogramme', nargs=1)
parser.add_argument('--exercice-spectrogramme-transparent', help="Génère un exercice à projeter", dest='transparent', nargs=1)
parser.add_argument('--exercice-spectrogramme-correction', help="Génère la correction pour l'enseignant", dest='correction', nargs=1)
parser.add_argument('--boomwhackers', help="Fichier décrivant la configuration de boomwhackers disponible.", dest='boomwhackers', nargs=1)
parser.add_argument('--primaire', help="Génère un exercice destiné aux primaires", dest='primaire', action='store_true',default=False)

args = parser.parse_args()

gamme = GammeTemperee(117)


boomwhackers = ""
if not args.boomwhackers is None:
  boomwhackers = args.boomwhackers[0]

# C: do
# D: ré
# E: mi
# F: fa
# G: sol
C = gamme.do(4)
D = gamme.re(4)
E = gamme.mi(4)
F = gamme.fa(4)
G = gamme.sol(4)

joie = [ [ [E, gamme.noire],
           [E, gamme.noire],
            [F, gamme.noire],
            [G, gamme.noire],
            [G, gamme.noire],
            [F, gamme.noire],
            [E, gamme.noire],
            [D, gamme.noire],
            [C, gamme.noire],
            [C, gamme.noire],
            [D, gamme.noire],
            [E, gamme.noire],
            [E, gamme.noirepointee],
            [D, gamme.croche],
            [D, gamme.blanche],
#            gamme.demiSoupir(),

            [E, gamme.noire],
            [E, gamme.noire],
            [F, gamme.noire],
            [G, gamme.noire],
            [G, gamme.noire],
            [F, gamme.noire],
            [E, gamme.noire],
            [D, gamme.noire],
            [C, gamme.noire],
            [C, gamme.noire],
            [D, gamme.noire],
            [E, gamme.noire],
            [D, gamme.noirepointee],
            [C, gamme.croche],
            [C, gamme.blanche], 
#            gamme.demiSoupir(),
        ] ]


if args.exerciceSpectrogramme or args.transparent or args.correction:
  g = GenerateurExercice(gamme.bpm)
  g.setBoomwhackers(boomwhackers)
  g.setLongueurPartitionColoree(8)
  g.setLongueurPartitionMusicale(16)
  if args.primaire:
    g.setTableauSimplifie(True)
  if args.exerciceSpectrogramme:
    g.genererExerciceSpectrogramme(joie, args.exerciceSpectrogramme[0], "thème 1")
  if args.transparent:
    g.genererTransparentSpectrogramme(joie, args.transparent[0], "thème 1", "Hymne à la joie")
  if args.correction:
    g.genererCorrectionSpectrogramme(joie, args.correction[0], "thème 1", "Hymne à la joie")
else:
  print("Génération du son")
  s = BasicSequenceur(1, gamme.bpm, 0)
  s.genererMelodie(joie, "sons/joie.wav")

